import Vue from 'vue'
import Electron from 'vue-electron'
import Resource from 'vue-resource'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './vuex/store'

Vue.use(Electron)
Vue.use(Resource)
Vue.use(BootstrapVue)

Vue.config.debug = true

window.appStart = Date.now() + 0

/* eslint-disable no-new */
new Vue({
  store,
  router,
  ...App
}).$mount('#app')
