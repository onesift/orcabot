/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "db" }] */
import { timestamp } from './bot/utils'
import { getActionMessage } from './utils'

let firebase, db

const setHeartbeat = () => {
  const botRef = uidRef().child('bot')

  botRef.child('heartbeat').set(timestamp())

  // update the linkedIn configuration status
  const linkedIn = JSON.parse(localStorage.getItem('linkedIn'))
  if (linkedIn && linkedIn.password) { // censor the password
    linkedIn.password = null
  }
  botRef.child('linkedIn').set(linkedIn)
}

if (process.env.NODE_ENV !== 'testing') {
  firebase = require('firebase')

  const config = {
    apiKey: 'AIzaSyBYMUBYmFURlXUNg5CKQQpfA-klmWziW7Y',
    authDomain: 'orca-31b91.firebaseapp.com',
    databaseURL: 'https://orca-31b91.firebaseio.com',
    projectId: 'orca-31b91',
    storageBucket: 'orca-31b91.appspot.com',
    messagingSenderId: '866795017632'
  }

  const fireApp = firebase.initializeApp(config)
  db = fireApp.database()

  window.firebase = firebase
  window.db = db

  // 3 heartbeats per minute
  setInterval(setHeartbeat, 20 * 1000)

  // user change event
  fireApp.auth().onAuthStateChanged(user => {
    if (user) {
      // track log changes
      uidRef().child('activity/lastLog').on('value', snap => {
        let ll = document.querySelector('.last-log')
        if (ll) {
          const val = snap.val()

          ll.innerText = val ? getActionMessage(val) : ''
        }

        ll = document.querySelector('.last-log2')
        if (ll) {
          const val = snap.val()
          ll.innerText = val ? getActionMessage(val) : ''
        }
      })
    }
  })
}

// get user id from Firebase
const uid = () => {
  if (firebase && firebase.auth) {
    if (firebase.auth().currentUser) {
      return firebase.auth().currentUser.uid
    }
  }
  return -1
}

// get user data from Firebase by user id
const uidRef = () => db.ref('users').child(uid())

export { firebase, db, uid, uidRef }
