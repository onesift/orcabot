export default {
  update: {
    title: 'Orca is updating. Wait a moment until it restarts.'
  },
  stepZero: {
    title: 'Engage and nuture leads automatically.',
    subTitle: 'Engage and build relationships before sending an email.',
    buttonText: 'GET STARTED',
    viewProfileText: 'View Profile',
    requestToConnectText: 'Request to Connect',
    sendEmailText: 'Send Email'
  },
  stepOne: {
    title: 'Connect your Linkedin account.',
    subTitle: 'Enter your credentials. Securely stored only on your computer.',
    buttonText: 'CONNECT LINKEDIN',
    verifyingText: 'Verifying account...',
    verifyingSucceedText: 'Success! Your Linkedin account is connected.'
  },
  stepTwo: {
    title: 'Connect your Google account.',
    subTitle:
      'Orca would like to view and manage your account to send emails and schedule meetings.',
    buttonText: 'CONNECT GMAIL',
    verifyingText: 'Verifying account...',
    verifyingSucceedText: 'Success! Your Google account is connected.'
  },
  stepThree: {
    title: 'Setup your campaign.',
    subTitle: 'Customize your campaign in three easy steps:',
    buttonText: 'CUSTOMIZE CAMPAIGN',
    stepOne: 'Step 1',
    stepOneDescription: 'Select a target user segment to engage and nature.',
    stepTwo: 'Step 2',
    stepTwoDescription:
      'Send a personalized message when connecting with leads.',
    stepThree: 'Step 3',
    stepThreeDescription:
      'Create a custom email template for outbound sales campaigns.'
  },
  stepFour: {
    title: '1. Select a target user segment.',
    subTitle:
      'Copy and paste the Linkedin search result URL into the form below.',
    verifyingText: 'Verifying linkedin url...',
    buttonText: 'GO TO STEP 2'
  },
  stepFive: {
    title: '2. Send a request to connect.',
    subTitle:
      'Compose a short personalized message to send when connecting with leads.',
    buttonText: 'GO TO LAST STEP',
    skipLinkText: 'Skip. Connect without a message.',
    proTipTitle: 'PRO TIP',
    proTipText:
      'Personalize messages by addressing leads by their first name with this magic snippet:',
    maxCharacters: '200'
  },
  stepSix: {
    title: '3. Send an email to your lead.',
    subTitle:
      'Now that you’ve connected and engaged with your lead, send a not-so-cold email.',
    buttonText: 'START THE CAMPAIGN',
    skipLinkText: 'Skip. Start the campaign later.',
    proTipTitle: 'PRO TIP',
    proTipText:
      'Personalize messages by addressing leads by their first name with this magic snippet:'
  },
  dashboard: {
    newCampaignText: '+ NEW CAMPAIGN'
  }
}
