import Vue from 'vue'
import Router from 'vue-router'
import { getFromStorage } from './utils'

/**
 * Auto updater code
 */
const { remote } = require('electron')
const simpleUpdater = remote.require('electron-simple-updater')

const ENV = process.env.NODE_ENV
const isProduction = ENV === 'production'
let updateAvailable = false

const onUpdateAvailable = meta => {
  updateAvailable = true
  goToUpdateView(router)
}
const onError = () => {
  updateAvailable = false
  goToWelcomeView(router)
}
const goToUpdateView = router => {
  router.push('update')
}
const goToWelcomeView = router => {
  router.push('welcome')
}

if (isProduction) {
  simpleUpdater.on('update-available', onUpdateAvailable)
  simpleUpdater.on('error', onError)
}
/**
 * End - Auto updater code
 */

Vue.use(Router)

let routes = [
  {
    path: '/',
    name: 'welcome',
    component: require('components/Welcome')
  },
  {
    path: '/linkedIn',
    name: 'linkedIn',
    component: require('components/steps/StepOne')
  },
  {
    path: '/update',
    name: 'update',
    component: require('components/Update')
  },
  {
    path: '/dashboard/:uid',
    name: 'dashboard',
    component: require('components/Dashboard'),
    meta: { auth: true }
  },
  {
    path: '/login',
    name: 'welcome',
    component: require('components/Welcome')
  },
  {
    path: '*',
    redirect: '/'
  }
]

let router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

window.router = router

router.beforeEach((to, from, next) => {
  console.log(to, from, next)
  let user = getFromStorage('user')
  if (updateAvailable) {
    if (to.name === 'update') {
      next()
    } else {
      next({ name: 'update' })
    }
  } else {
    if (user && user.uid) {
      // if (user.hasCampaigns) {
      if (from.name === null && to.name === 'welcome') {
        next({ name: 'dashboard', params: { uid: user.uid } })
      } else if (
        from.name === 'dashboard' &&
        to.name === 'welcome' &&
        !to.query.newCampaign
      ) {
        // do nothing!!!
      } else {
        next()
      }
      // } else {
      // next()
      // }
    } else {
      if (to.matched.some(record => record.meta.auth)) {
        next({ name: 'login' })
      } else {
        next()
      }
    }
  }
})

export default router
