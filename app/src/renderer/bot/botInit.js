import randomUa from 'random-useragent'
import electronLog from 'electron-log'

const CHROME_LATEST_VERSION = 55 - 1 // (max - min stable)
const FIREFOX_LATEST_VERSION = 51 - 3 // (max - min stable)

const BROWSER_INFO = randomUa.getRandomData(ua => {
  // gets browser info of browsers for Windows, Mac OS, Linux
  // only Firefox and Chrome versions grater than
  // FIREFOX_LATEST_VERSION and than CHROME_LATEST_VERSION
  return (
    ((ua.browserName === 'Firefox' &&
      parseFloat(ua.browserVersion) >= FIREFOX_LATEST_VERSION) ||
      (ua.browserName === 'Chrome' &&
        parseFloat(ua.browserVersion) >= CHROME_LATEST_VERSION)) &&
    (ua.osName === 'Windows' || ua.osName === 'Mac OS' || ua.osName === 'Linux')
  )
})

electronLog.info(BROWSER_INFO)

let Nightmare
let showBot = false // true
let electronPath

// if (process.env.NODE_ENV !== 'testing') {
let remote = require('electron').remote
Nightmare = require('nightmare')
let path = require('path')

if (process.env.NODE_ENV === 'development') {
  showBot = true
  electronPath = remote.app.getPath('exe')
} else {
  // production build has different paths, based on platform
  if (process.platform === 'linux') {
    electronPath = path.join(
      remote.app.getAppPath() + '.unpacked',
      'node_modules',
      'electron',
      'dist',
      'electron'
    )
  } else if (process.platform === 'darwin') {
    let folder = remote.app.getPath('exe').split('/')
    folder.pop()
    folder.pop()
    electronPath = path.join(
      ...folder,
      'Resources',
      'app.asar.unpacked',
      'node_modules',
      'electron',
      'dist',
      'Electron.app',
      'Contents',
      'MacOS',
      'Electron'
    )
  } else {
    let folder = remote.app.getPath('exe').split('\\')
    folder.pop()
    electronPath = path.join(
      ...folder,
      'resources',
      'app.asar.unpacked',
      'node_modules',
      'electron',
      'dist',
      'electron.exe'
    )
  }
}

Nightmare.action('connectedUserIds', function (done) {
  console.log('get last connected user ids')
  this.evaluate_now(function () {
    var result = []
    var links = document.querySelectorAll(
      'li.connection-card a.mn-person-info__link'
    )
    for (var i = 0; i < links.length; ++i) {
      result.push(links[i].href.substr(28).slice(0, -1))
    }
    return result
  }, done)
})

// add custom nightmare actions here aka pipeline actions
// just defined new action like I did with view/connect/checkConnect
Nightmare.action('performAction', {
  view (message, done) {
    this.evaluate_now(function () {
      let fullName = document
        .querySelector('.pv-top-card-section__name')
        .textContent.trim()
      return { status: true, fullName: fullName }
    }, done)
  },
  connect (message, done) {
    // TODO: FIx message not passed in this.evaluate_now func
    this.evaluate_now(function () {
      let fullName = document
        .querySelector('.pv-top-card-section__name')
        .textContent.trim()
      let frontFacedButton = document.querySelector('button.connect')
      let connectInMenuButton = document.querySelectorAll(
        '.closed .actions li.connect'
      )
      if (frontFacedButton) {
        // open up connect modal
        frontFacedButton.click()
        if (message) {
          // replace first_name with real first_name
          let parsedMessage = message.replace(
            '{{first_name}}',
            fullName.split(' ')[0]
          )
          setTimeout(() => {
            // click on 'ADD A NOTE'
            document
              .querySelector(
                '.modal-wormhole.send-invite .send-invite__actions .button-secondary-large'
              )
              .click()
            setTimeout(() => {
              // write message to textarea
              document.querySelector(
                '.modal-wormhole.send-invite textarea[name="message"]'
              ).value = parsedMessage
              document
                .querySelector(
                  '.modal-wormhole.send-invite .send-invite__actions .button-primary-large'
                )
                .click()
              return { status: true, connected: false }
            }, 1000)
          }, 1000)
        } else {
          // TODO: Uncomment this below to actually send the connect
          // document.querySelector('.modal-wormhole.send-invite .send-invite__actions .button-primary-large').click()
          console.log('SENT INVITE')
          return { status: true, connected: false }
        }
      } else if (connectInMenuButton) {
        // open up connect modal from menu
        connectInMenuButton.click()
        if (message) {
          // replace first_name with real first_name
          let parsedMessage = message.replace(
            '{{first_name}}',
            fullName.split(' ')[0]
          )
          setTimeout(() => {
            // click on 'ADD A NOTE'
            document
              .querySelector(
                '.modal-wormhole.send-invite .send-invite__actions .button-secondary-large'
              )
              .click()
            setTimeout(() => {
              // write message to textarea
              document.querySelector(
                '.modal-wormhole.send-invite textarea[name="message"]'
              ).value = parsedMessage
              document
                .querySelector(
                  '.modal-wormhole.send-invite .send-invite__actions .button-primary-large'
                )
                .click()
              return { status: true, connected: false }
            }, 1000)
          }, 1000)
        } else {
          // TODO: Uncomment this below to actually send the connect
          // document.querySelector('.modal-wormhole.send-invite .send-invite__actions .button-primary-large').click()
          console.log('SENT INVITE')
          return { status: true, connected: false }
        }
      } else {
        // no connect button is found, return status false
        return { status: false, connected: false }
      }
    }, done)
  },
  checkConnectStatus (message, done) {
    this.evaluate_now(function () {
      // if pendingElem exist then we are still waiting for conection
      let pendingElem = document.querySelector('.invitation-pending')
      return { status: true, connected: !pendingElem }
    }, done)
  }
})
// }

function createBot () {
  return {
    electronPath: electronPath,
    x: 0,
    y: 0,
    alwaysOnTop: true,
    // uncomment this to open dev tools on bot window
    // openDevTools: {mode: 'detach'},
    // set persistence between instances
    // this means all nightmare instances will have linkedin account already logged in
    // when you want to test new linkedin account, just set partition to 'nopersist'
    width: 1024,
    height: 800,
    title: BROWSER_INFO.browserName,
    // type: 'desktop',
    webPreferences: {
      // use a random number for session to clear data
      partition: 'persist:1'
      // nativeWindowOpen: true
    },
    // how long to wait for some element to apear (in ms)
    waitTimeout: 20 * 1000, // 20 seconds
    // showBot is defined based on NODE_ENV
    show: showBot,
    useragent: BROWSER_INFO.userAgent
  }
}

export default createBot
