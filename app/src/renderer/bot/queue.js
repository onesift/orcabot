import { ACTIONS } from './availableActions'
import * as limits from './dailyLimits'

// array of actions that will be proccessed in order how they are added
const QUEUE = []

// push new action to the end of queue array
// proccessing of this action will start once all actions before this one are proccessed
function addToQueue (item) {
  if (typeof item === 'object' && !Array.isArray(item)) {
    // check if item has action property and is not empty
    if (!item.action || item.action.length === 0) {
      throw new Error('Action name must be defined.')
    }
    // check if item action name is included in available actions
    if (ACTIONS.includes(item.action)) {
      let itemsInQueue = QUEUE.filter(elem => {
        return elem.action === item.action
      }).length
      let dailyLimit = limits[item.action.toUpperCase()]
      // check if adding this action will not exceed daily limit for this action
      if (dailyLimit > itemsInQueue) {
        QUEUE.push(item)
      } else {
        throw new Error(
          `You will exceed daily ${item.action.toUpperCase()} limit. You already have ${itemsInQueue} items in QUEUE.`
        )
      }
    } else {
      throw new Error(`Unknown action ${item.action}.`)
    }
  } else {
    throw new Error('Only objects can be added to QUEUE.')
  }
}

// get first action from queue to be proccessed
function getFromQueue () {
  if (QUEUE.length > 0) {
    return QUEUE.shift()
  } else {
    throw new Error('QUEUE is empty.')
  }
}

// clear everything from QUEUE
function clearQueue () {
  QUEUE.length = 0
}

export { QUEUE, addToQueue, getFromQueue, clearQueue }
