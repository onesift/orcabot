// actions available to pipeline
const VIEW = 'view'
const CONNECT = 'connect'
const EMAIL = 'email'

const ACTIONS = [VIEW, CONNECT, EMAIL]

export { ACTIONS, VIEW, CONNECT, EMAIL }
