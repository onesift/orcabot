import createBot from './botInit'
import { db, uid, uidRef } from '../firebase'
import { isProduction, prompt, random, timestamp } from './utils'

// import google from 'googleapis'
import nightmare from 'nightmare'
import electronLog from 'electron-log'

// config for googleapis
// const gmail = google.gmail('v1')

// config for electron-log
if (isProduction) {
  // electronLog.transports.file.level = 'info'
  // electronLog.transports.console.level = 'info'
}

// constants Linkedin links
const LOGIN_PAGE = 'https://www.linkedin.com/uas/login'
const HOME_PAGE = 'https://www.linkedin.com/feed/'
const LOGIN_USER_AGENT = createBot().useragent // 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36' // 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
// const SEARCH_PAGE = 'https://www.linkedin.com/search/results/people/'

// constant idle interval
const MIN_IDLE_MINUTES = 10
const MAX_IDLE_MINUTES = 30
const heartbeatInterval = window.setInterval(() => {
  log('app is idle for more than 30 minutes, sending heartbeat')
}, MAX_IDLE_MINUTES * 60 * 1000)

// activity log variables
// const activityLogElement = document.getElementById('logDiv')
let activityLogContinue = false
let activityLogNextRun = false
let latestCampaignKey = -1

// variables
let bot
const botSettings = createBot()
let campaignKey = JSON.parse(window.localStorage.getItem('campaignKey')) || -1

const run = {
  // BOT RUN CONFIGURATION
  getRepeatDelay () {
    // wait for random minutes before repeat run [from, to]
    return random(MIN_IDLE_MINUTES, MAX_IDLE_MINUTES)
  },

  heartbeatInterval,

  // enter login data
  login: { enabled: true },

  // check which connections were accepted
  // acquire emails of accepted targets if missing
  connectCheck: { enabled: true },

  // search for new targets
  search: { enabled: true },

  // visit random targets
  visit: {
    enabled: true,
    repeat: random(23, 78), // random number of repeats [from, to]
    speed: 0.1, // time limit in hours between steps

    connect: true, // try to connect to the target
    email: true // try to get email address after connecting
  },

  // send email to targets with email
  email: { enabled: true }
}

const getMacroFromDatabase = async name =>
  db.ref('bot').child('macros').child(name).once('value').then(re => re.val())

const getSearchUrl = async campaign => {
  if (campaign) {
    return uidRef()
      .child('campaigns')
      .child(campaign)
      .child('url')
      .once('value')
      .then(re => re.val())
  }
  return -1
}

const getChatMessage = async campaign =>
  uidRef()
    .child('campaigns')
    .child(campaign)
    .child('body')
    .once('value')
    .then(_ => _.val())

const startBot = async _ => {
  bot = nightmare(botSettings)
  await bot.useragent(await userAgent())
  window.botActive = true
  return bot
}

const endBot = async _ => {
  window.botActive = false
  return bot.end().then(_ => _)
}

const prepareMacro = async params => {
  let [name, p0, p1, p2] = params.split(';')
  let firstName, lastName, targetName
  let macro = await getMacroFromDatabase(name)
  console.log('using macro:', name, macro)
  campaignKey = JSON.parse(window.localStorage.getItem('campaignKey'))

  if (name === 'search' && p0 && p1) {
    // get current search url
    p2 = await getSearchUrl(campaignKey)
  }

  if (name === 'chat' && p0) {
    targetName = await uidRef()
      .child('targetName')
      .child(campaignKey)
      .child(p0)
      .once('value')
      .then(_ => _.val())
    firstName = targetName.split(' ')[0]
    lastName = targetName.split(' ')[1]

    p1 = await getChatMessage(campaignKey)
    p1 = p1.replace(/\{\{first_name\}\}/gi, firstName)
    p1 = p1.replace(/\{\{last_name\}\}/gi, lastName)
    p1 = p1 + '@@'
    console.log({ firstName, p0, p1 })
  }

  // insert macro settings
  macro = macro.replace(/\$0/g, p0)
  macro = macro.replace(/\$1/g, p1)
  macro = macro.replace(/\$2/g, p2)

  console.log('executing script:', { macro, params })
  return macro
}

const botRemoteControl = async (commandList, commandRef) => {
  if (!commandList) return

  const commands = commandList.split(',')

  for (let val of commands) {
    let [command, params] = val.split('(')
    if (params) {
      params = params.slice(0, -1)
    }

    let text, target, desc, start, count

    switch (command) {
      case 'macro':
        const macro = await prepareMacro(params)
        await botRemoteControl(macro)
        break

      case 'start':
        if (window.botActive) {
          // if active kill old bot before starting new one
          await endBot()
        }
        await startBot()
        break

      case 'end':
        await endBot()
        break

      case 'open':
        await bot.goto(params)
        break

      case 'connect':
        await tryToConnect(params)
        break

      case 'wait':
        await bot.wait(params)
        break

      case 'type':
        ;[text, target, desc] = params.split(';')
        text = text.replace(/@@/g, String.fromCharCode(13))
        await type({ text, target, desc })
        break

      case 'scroll':
        await scrollToLoad()
        break

      case 'xfromsearch':
        ;[start, count] = params.split(';')
        await extractFromSearch({ start, count })
        break

      case 'click':
        ;[target, desc] = params.split(';')
        await click({ target, desc })
        break

      case 'logout_linked_in':
        window.localStorage.clear()
        window.location.reload()
        break

      case 'login_linked_in':
        window.router.push('/linkedIn')
        break
    }
  }

  // if Firebase reference available
  if (commandRef) {
    console.log('removing timeslot ', commandRef.key)
    // delete command at the end of execution
    await commandRef.remove()
  }
}

window.botRemoteControl = botRemoteControl

setTimeout(_ => {
  uidRef().child('bot').on('child_changed', async snapshot => {
    if (snapshot.key === 'command') {
      await botRemoteControl(snapshot.val())
    }
  })
}, 3000)

const getDate = () => {
  const d = new Date()
  return d.getUTCDay() * 24 * 60 + d.getUTCHours() * 60 + d.getUTCMinutes()
}

let prevTime = -1
setInterval(async _ => {
  if (uid() === -1) return

  const time = getDate()

  if (prevTime !== time) {
    // prevent repeating same command multiple times
    prevTime = time

    const runningRef = uidRef().child('campaignRunning')

    runningRef.once('value').then(snapshot =>
      // loop over all running campaigns
      snapshot.forEach(async ({ key }) => {
        // fetch current time slot
        const commandRef = uidRef().child('botSchedule').child(key).child(time)

        // fetch value in time slot
        const command = await commandRef.once('value').then(async _ => _.val())

        if (command === null) {
          // no command for this time
          console.log(time, key, 'nothing to do')
          return
        }

        console.log(time, key, command)
        campaignKey = key
        window.localStorage.setItem('campaignKey', JSON.stringify(key))
        botRemoteControl(command, commandRef)
      })
    )
  }
}, 1000)

// log information to electronLog and to Firebase
const log = async txt => {
  campaignKey = JSON.parse(window.localStorage.getItem('campaignKey'))
  electronLog.info(txt)
  if (campaignKey) {
    try {
      return (
        uidRef()
        .child('botRun')
        .child(campaignKey)
        .child('log')
        .child((new Date().valueOf() / 1000) | 0)
        .set(txt) && uidRef().child('lastActive').set(new Date().valueOf())
      )
    } catch (err) {
      return console.log('Log has no connection to Firebase: ' + err)
    }
  }
}

// log action to electronLog and to Firebase
const logAction = async obj => {
  campaignKey = JSON.parse(window.localStorage.getItem('campaignKey'))
  electronLog.info(obj)
  if (campaignKey) {
    try {
      await uidRef()
        .child('botRun')
        .child(campaignKey)
        .child('action')
        .child((new Date().valueOf() / 1000) | 0)
        .set(obj)

      await uidRef().child('lastActive').set(new Date().valueOf())

      return uidRef().child('activity').child('lastLog').set(obj)
    } catch (err) {
      return console.log('Log has no connection to Firebase: ' + err)
    }
  }
}

// get user agent from Firebase
const userAgent = async _ =>
  db.ref('browser').once('value').then(async _ => _.val())

// bot start is triggered with the logIn call
const logIn = async (
  email,
  password,
  onlyLogin = false,
  { setToStorage, dispatch, commit, types } = {}
) => {
  // set lastActive so we will know that user runs the app
  uidRef().child('lastActive').set(new Date().valueOf())

  campaignKey = JSON.parse(window.localStorage.getItem('campaignKey'))
  const hasCampaignKeyChanged = latestCampaignKey !== campaignKey

  if (onlyLogin) {
    // create an electron instance for the bot
    bot = nightmare(botSettings)
    bot.useragent(LOGIN_USER_AGENT)
    try {
      bot.useragent(await userAgent())
    } catch (err) {
      electronLog.warn('Bot user agent error: ' + err)
    }
    commit(types.TOGGLE_LINKED_IN_VERIFYING)
    await linkedInLogin(email, password)
      .then(() => {
        setToStorage('linkedIn', { email, password })
        dispatch(
          'fadeOut',
          document.querySelector('.step-wrapper')
        ).then(() => {
          commit(types.SET_LINKED_IN_CREDENTIALS_VALID)
          dispatch(
            'fadeIn',
            document.querySelector('.step-wrapper')
          ).then(() => {
            setTimeout(() => {
              dispatch('incrementStep')
            }, 3000)
          })
        })
      })
      .catch(() => {
        commit(
          types.SET_LINKED_IN_ERROR,
          'Incorrect login credentials, please try again.'
        )
        commit(types.TOGGLE_LINKED_IN_VERIFYING)
      })
  }

  // get enabled value from Firebase
  let enabled
  try {
    enabled = await uidRef()
      .child('enabled')
      .once('value')
      .then(async _ => _.val())

    if (enabled === null) {
      // if key does not exist set enabled=true and continue
      uidRef().child('enabled').set(true)
    } else {
      if (enabled !== true) {
        return log('account not enabled, aborting')
      }
    }
  } catch (error) {
    electronLog.log(error)
  }

  // remove any existing timer
  clearTimeout(window.localStorage.getItem('autorun'))

  const ckRef = _ => {
    if (!campaignKey) {
      campaignKey = -1
    }
    return uidRef().child('campaigns').child(campaignKey)
  }

  const now = (new Date().valueOf() / 1000) | 0

  ckRef().child('lastRun').set(now)
  ckRef().child('lastRunTxt').set(new Date().toUTCString())

  const nextRunRef = ckRef().child('nextRun')
  const nextRun = await nextRunRef.once('value').then(async snap => snap.val())

  let activityLogElement = document.getElementById('logDiv')

  if (!activityLogElement) {
    activityLogElement = document.querySelector('.last-log')
  }

  if (activityLogElement) {
    uidRef()
      .child('botRun')
      .child(campaignKey)
      .child('action')
      .on('child_added', child => {
        const key = parseInt(child.key, 10) * 1000
        const val = child.val()
        const { action, target, targetName, email } = val
        let txt = ''

        const ts = new Date(key).toLocaleString().split(':')
        const timestamp = ts.join(':') + ' '

        if (action === 'start') {
          txt = 'Orca session initiated at ' + timestamp + txt
        } else if (action === 'connect') {
          txt =
            timestamp +
            'Sent ' +
            targetName +
            ' a connection request ' +
            '(<a target="_blank" href="http://linkedin.com/in/' +
            target +
            '">http://linkedin.com/in/' +
            target +
            '</a>)' +
            txt
        } else if (action === 'connected') {
          txt =
            timestamp +
            'Connected with ' +
            targetName +
            ' (<a target="_blank" href="http://linkedin.com/in/' +
            target +
            '">http://linkedin.com/in/' +
            target +
            '</a>)' +
            txt
        } else if (action === 'discovered') {
          txt =
            timestamp +
            'Discovered ' +
            targetName +
            ' (<a target="_blank" href="http://linkedin.com/in/' +
            target +
            '">http://linkedin.com/in/' +
            target +
            '</a>)' +
            txt
        } else if (action === 'visited') {
          txt =
            timestamp +
            'Visited ' +
            targetName +
            '\'s profile (<a target="_blank" href="http://linkedin.com/in/' +
            target +
            '">http://linkedin.com/in/' +
            target +
            '</a>)' +
            txt
        } else if (action === 'emailGet') {
          txt =
            timestamp +
            'Got email ' +
            email +
            ' from ' +
            targetName +
            '\'s profile (<a target="_blank" href="http://linkedin.com/in/' +
            target +
            '">http://linkedin.com/in/' +
            target +
            '</a>)' +
            txt
        } else if (action === 'emailSent') {
          txt =
            timestamp + 'Sent email to ' + targetName + ' (' + email + ')' + txt
        } else {
          txt = JSON.stringify(val) + txt
        }

        txt = txt + '\n'

        activityLogElement.innerHTML = txt + activityLogElement.innerHTML
      })

    if (hasCampaignKeyChanged) {
      activityLogElement.innerHTML = ''
    }

    if (nextRun && nextRun > now) {
      electronLog.info('time of nextRun is in the future, aborting')
      // add info to recent activity log if there is planned session

      setTimeout(() => {
        if (
          (activityLogContinue && !activityLogNextRun) ||
          hasCampaignKeyChanged
        ) {
          activityLogElement.innerHTML =
            'Selected campaign is waiting for the next planned run, do not close the app.' +
            '\n' +
            'Identifier of the campaign: ' +
            campaignKey +
            '\n' +
            activityLogElement.innerHTML
        } else if (!activityLogNextRun || hasCampaignKeyChanged) {
          activityLogElement.innerHTML =
            'Selected campaign is waiting for the next planned run, do not close the app.' +
            '\n' +
            'Identifier of the campaign: ' +
            campaignKey +
            '\n' +
            activityLogElement.innerHTML
        }
        latestCampaignKey = campaignKey
        activityLogNextRun = true
      }, 1000)
      window.botActive = false
      return
    } else if (!activityLogContinue) {
      // clear recent activity log
      activityLogElement.innerHTML = ''
    }
  }

  log('new campaign started at ', new Date())

  // create an electron instance for the bot
  bot = nightmare(botSettings)
  bot.useragent(await userAgent())

  window.botActive = true

  const next = now + run.getRepeatDelay() * 60 // minutes to seconds
  nextRunRef.set(next) // next run at least X seconds in the future
  ckRef().child('nextRunTxt').set(new Date(1000 * next).toUTCString())

  logAction({ action: 'start' })

  await bot
    .goto('http://linkedin.com')
    .wait(1000)
    .evaluate(function () {
      // check if logged in from before
      return document.querySelectorAll('.nav-main__content').length === 0
    })
    .then(async notLoggedIn => {
      if (run.login.enabled && notLoggedIn) {
        await linkedInLogin(email, password)
      }
    })

  if (run.connectCheck.enabled) await checkConnected()

  if (run.search.enabled) {
    const url = JSON.parse(window.localStorage.getItem('url'))
    if (url) await searchFor({ prefix: url })
    await extractFromSearch()
  }

  if (run.visit.enabled) await visitRandomProfiles(run.visit.repeat)

  if (run.email.enabled) {
    const noEmail = await ckRef()
      .child('noEmail')
      .once('value')
      .then(async _ => _.val())

    if (noEmail === true) {
      // email sending blocked
      log('emailing disabled')
    } else {
      await emailToConnected()
    }
  }

  // const minDelay = 1000 * 60 * 30 // 30 min
  // const maxDelay = 1000 * 60 * 180 // 3 h
  // const diff = maxDelay - minDelay
  // const randomDelay = minDelay + diff * Math.random()

  // console.log('restarting in ' + ((randomDelay / 1000 / 60) | 0) + ' minutes')

  // const autorun = setTimeout(() => {
  //   logIn(email, password, false)
  // }, randomDelay)

  // store reference to new timer
  // window.localStorage.setItem('autorun', autorun)

  bot.wait(1).catch(err => console.error(err)).then(_ => {
    // finally cleanup
    bot.end().then(re => re)
    // kill the Electron process explicitly to ensure no orphan child processes
    bot.proc.disconnect()
    bot.proc.kill()
    bot.ended = true
    bot = null
  })

  window.botActive = false
  activityLogContinue = true
  activityLogNextRun = false

  return true
}

const gmailSend = async ({ to, subject, body, targetId, campaignKey }) =>
  uidRef().child('mail').push().set({
    to,
    body,
    subject,
    targetId,
    campaignKey
  })

const emailToConnected = async () => {
  // find all connected
  const targetEmailRef = uidRef().child('targetEmail').child(campaignKey)

  let keys = []
  await targetEmailRef.once('value').then(async snapshot => {
    const data = snapshot.val()
    if (data) {
      keys = Object.keys(data)
    }
  })

  const emailsSentRef = uidRef()
    .child('campaigns')
    .child(campaignKey)
    .child('emailsSent')

  // send to all not yet mailed
  keys.forEach(async key => {
    const sent = await getEmailSentTimestamp(key)
    const to = await getEmail(key)

    if (sent === null) {
      const name = await getName(key)
      let subject = JSON.parse(window.localStorage.getItem('subject'))
      let body = JSON.parse(window.localStorage.getItem('body'))

      subject = subject.replace('{{first_name}}', name.split(' ')[0])
      subject = subject.replace('{{last_name}}', name.split(' ')[1])

      body = body
        .replace(/(?:\r\n|\r|\n)/g, '<br />')
        .replace('{{first_name}}', name.split(' ')[0])
        .replace('{{last_name}}', name.split(' ')[1])

      emailsSentRef.transaction(count => {
        if (!count) return 1
        count++
        return count
      })

      gmailSend({ to, subject, body, targetId: key, campaignKey })
      logAction({
        action: 'emailSent',
        target: key,
        targetName: name,
        email: to
      })
    } else {
      log('allready sent ' + to)
    }
  })
}

const getValue = async ({ field, key }) => {
  const ref = uidRef().child(field).child(campaignKey).child(key)
  const val = await ref.once('value').then(async snap => snap.val())
  return val
}

const getEmailSentTimestamp = async key =>
  getValue({ field: 'targetEmailSent', key })

const getEmail = async key => getValue({ field: 'targetEmail', key })

const getConnectTimestamp = async key =>
  getValue({ field: 'targetConnect', key })

const getConnectedTimestamp = async key =>
  getValue({ field: 'targetConnected', key })

const getDiscoveredTimestamp = async key =>
  getValue({ field: 'targetDiscovered', key })

const getVisitedTimestamp = async key =>
  getValue({ field: 'targetVisited', key })

const getName = async key => getValue({ field: 'targetName', key })

const tryToConnect = async key => {
  console.group('try to connect')
  log('try to connect')

  // check if allready sent
  const sent = await getConnectTimestamp(key)

  if (sent === null) {
    const name = await getName(key)

    const msg = JSON.parse(window.localStorage.getItem('message'))
    let message = msg.replace('{{first_name}}', name.split(' ')[0])
    message = message.replace('{{last_name}}', name.split(' ')[1])

    // wait for page to load
    await bot.wait(2000)

    if (await bot.exists('button.connect')) {
      // connect button visible
      await click({
        desc: 'connect',
        target: 'button.connect'
      })
    } else {
      // connect button in menu
      await click({
        desc: 'menu',
        target: '.core-rail button.dropdown-trigger'
      })
      await click({
        desc: 'connect',
        target: 'li.connect'
      })
    }

    await click({
      desc: 'add a note',
      target: 'button.button-secondary-large'
    })

    await type({
      text: message,
      target: '#custom-message',
      desc: 'message'
    })

    await click({
      desc: 'send',
      target: 'button.button-primary-large'
    })

    const path = 'users/' + uid() + '/targetConnect/' + campaignKey + '/'

    db.ref(path).update({ [key]: timestamp() })

    logAction({ action: 'connect', target: key, targetName: name })

    await bot.wait(random(1000, 2000))

    await click({
      desc: 'send',
      target: 'button.button-primary-large'
    })
  } else {
    log('allready sent ' + key)
  }

  console.groupEnd('try to connect')
}

const promptAndType = async ({ title, description, target, desc }) => {
  try {
    let text = prompt(title, description)
    log('typing into ' + desc + ' ' + text)
    await bot
      .wait(target)
      .wait(random(500, 1500))
      .type(target, '') // clear input
      .type(target, text) // simulate typing
      .type(target, '') // clear input
      .insert(target, text) // to keep newlines and override other entry errors
  } catch (e) {
    console.error(e)
    return false
  }
  return true
}

const type = async ({ text, target, desc }) => {
  try {
    if (desc === 'password') {
      log('typing into ' + desc + ' ' + '***')
    } else {
      log('typing into ' + desc + ' ' + text)
    }

    await bot
      .wait(target)
      .wait(random(500, 1500))
      .type(target, '') // clear input
      .type(target, text) // simulate typing
      .type(target, '') // clear input
      .insert(target, text) // to keep newlines and override other entry errors
  } catch (e) {
    console.error(e)
    return false
  }
  return true
}

const click = async ({ target, desc }) => {
  try {
    log('click on ' + desc)
    await bot.wait(target).wait(random(500, 1500)).click(target)
  } catch (e) {
    console.error(e)
    return false
  }
  return true
}

const url = async ({ url, desc }) => {
  try {
    log('open ' + desc + ' ' + url)
    await bot.goto(url)
  } catch (e) {
    console.error(e)
    return false
  }
  return true
}

/**
  `Sign-In Verification
  This login attempt seems suspicious. Please enter the verification code we sent to your email address or phone to finish signing in.``
*/
const linkedInLoginConfirmCode = async () => {
  log('confirmation code request')
  // TODO: translation i18n
  await promptAndType({
    title: 'Sign-In Verification',
    description:
      'Please enter the verification code that Linkedin sent to your email address or phone to finish signing in.',
    desc: 'password',
    target: '#verification-code'
  })
  log('confirmation code confirm')
  await click({
    desc: 'submit',
    target: '#btn-primary'
  })
  return bot.wait(3000)
}

const linkedInLogin = async (email, password) => {
  console.group('logIn')

  if (!email || !password) {
    const saved = JSON.parse(window.localStorage.getItem('linkedIn'))
    email = saved.email
    password = saved.password
  }

  await url({
    desc: 'login page',
    url: LOGIN_PAGE
  })

  await bot.wait(500)

  await type({
    desc: 'email',
    target: '#session_key-login',
    text: email
  })

  await bot.wait(500)

  await type({
    desc: 'password',
    target: '#session_password-login',
    text: password
  })

  await bot.wait(500)

  await click({
    desc: 'sign in',
    target: '#btn-primary'
  })

  await bot.wait(500)

  log('wait for post button to confirm login')

  console.groupEnd('logIn')
  console.group('logInConfirm')

  await bot.wait(500) // wait for error to show if login failed

  // if page still loading wait max 30s
  // function responds with true if logged in

  await bot.exists('.alert.error').then(result => {
    // if error abort
    if (result) {
      log('login error detected')
      // window.alert('Login failed')
      // INFO: turned off because of an error:
      // "You have exceeded the maximum number of code requests."
    }
  })

  if (await bot.exists('#verification-code')) {
    await linkedInLoginConfirmCode()

    await bot.wait(1000)

    await bot.exists('.alert.error').then(result => {
      return linkedInLoginConfirmCode()
    })
  }

  log('login before end')
  console.groupEnd('logInConfirm')
  return bot.wait('.nav-main__content')
}

const searchFor = async ({ prefix, data = {}, key = '' }) => {
  /* let query

  if (data && key) {
    query = data[key]
  } else {
    query = prefix
      .split('/')
      .pop()
      .replace('?', '')
      .split('&')
      .shift()
      .split('=')
      .pop()
  } */

  // console.group('search for ' + query)

  // basic search
  // await type({
  //   desc: 'search query',
  //   target: 'div[role=search] input',
  //   text: query
  // })

  // await click({
  //   desc: 'search icon',
  //   target: 'button.nav-search-button'
  // })

  // await click({
  //   desc: 'people link',
  //   target: 'button[data-vertical=PEOPLE]'
  // })

  // wait for page to settle
  // await bot.wait(3000)

  // open real url
  await url({
    desc: 'search url',
    url: prefix + key
  })

  // advanced filtering

  /*
  if (window.state3.url2 !== '') {
    await click({
      desc: 'keywords sidebar',
      target: 'li.search-facet--advanced-search-filters h3'
    })

    await type({
      desc: 'search title',
      target: '#advanced-search-title',
      text: window.state3.url2
    })

    await bot.type('#advanced-search-title', '\u000d')

    // wait for page to settle
    await bot.wait(5000)
  }

  await click({
    desc: 'locations sidebar',
    target: 'li.search-facet--geo-region h3'
  })

  await click({
    desc: 'add location link',
    target: 'li.search-s-add-facet button'
  })

  await bot
    .wait('.search-s-facet-value input')
    .type('.search-s-facet-value input', window.state3.url3)
    .wait(2000) // TODO: trigger lookup

  await click({
    desc: 'click on first suggestion',
    target: '.type-ahead-result.location'
  })
*/

  // console.groupEnd('search for ' + query)
}

const extractFromSearch = async ({ start = 1, count = 1 } = {}) => {
  const startFromPage = parseInt(start, 10)
  count = parseInt(count, 10) || 1

  const numPages = ((await numSearchResults()) / 10) | 0

  console.log({ startFromPage, numPages, count })

  if (startFromPage > numPages) {
    return
  }

  log('parse page of results ' + startFromPage + '/' + count + ' pages')

  await scrollToLoad()
  await parseSearchResults()

  let i = startFromPage
  while (i < startFromPage + count && i < numPages) {
    console.log({ i, startFromPage, count, numPages })

    const campaignKey = JSON.parse(window.localStorage.getItem('campaignKey'))
    const surl =
      'https://www.linkedin.com/search/results/' +
      (await getSearchUrl(campaignKey)) +
      '&page=' +
      i
    await bot.goto(surl)
    // await botRemoteControl('macro(search;' + i + ';1)')
    await scrollToLoad()
    await parseSearchResults()
    i = i + 1
  }

  // schedule search with page offset
  console.log({ i, numPages })
  if (i < numPages) {
    const campaignKey = JSON.parse(window.localStorage.getItem('campaignKey'))
    const time = getDate()

    console.log({ campaignKey, time })

    uidRef()
      .child('botSchedule')
      .child(campaignKey)
      .child(time + 60)
      .set(`start,macro(search;${i};${count}),end`)
  }

  /* let i = 1
  const ckRef() = uidRef().child('campaigns').child(campaignKey)

  if (!startFromPage) {
    let dcnt = await ckRef().child('discoveredCount').once('value').then(getSnap)
    startFromPage = (dcnt / 10) | 0
  }

  if (startFromPage > 0) {
    i = startFromPage
  }

  while (i <= numPages) {
    console.group('parse page ' + i)

    if (i !== 1) {
      // click next if not page 1
      await click({
        desc: 'go to page ' + i + ' of results',
        target: 'button.next'
      })
    }

    await scrollToLoad()
    await parseSearchResults()

    if (i === 1) {
      // parse total number of results
      const count = await findTotalSearchResults()
      const maxPages = 1 + ((count / 10) | 0)

      ckRef().child('prospects').set(count)
      ckRef().child('searchResultPageCount').set(maxPages)
    }

    console.groupEnd('parse page ' + i)
    i++
  } */

  return true
}

/* const findTotalSearchResults = async () => {
  let re = 1
  console.log(re)
  await bot
    .evaluate(function () {
      const txt = document.querySelector('.search-results__total').innerText
      const val = parseInt(txt.split(' ')[1].replace(/,/g, ''), 10)
      return val
    })
    .then(data => {
      re = data
    })
  console.log(re)
  return re
} */

const parseSearchResults = async () => {
  log('parse results')
  await bot
    .evaluate(function () {
      let names = {}
      document
        .querySelectorAll('.search-result__info > a')
        .forEach(userLink => {
          const name = userLink
            .querySelector('.actor-name')
            .textContent.split(',')[0]

          const url = userLink.href
          const key = url.substr(28).slice(0, -1)

          if (key.split('/')[0] !== 'rch') {
            // ignore trash entries
            names[key] = { name }
          }
        })
      return names
    })
    .then(updateNames)
}

const updateNames = async targetData => {
  log(targetData)
  const path = 'users/' + uid() + '/targetName/' + campaignKey + '/'
  const path2 = 'users/' + uid() + '/targetDiscovered/' + campaignKey + '/'

  await Object.keys(targetData).forEach(async key => {
    db.ref(path).update({ [key]: targetData[key].name })
    log('updating name ' + key + ' ' + targetData[key].name)

    // update discovery only if it doesn't exist from before
    const discovered = await getDiscoveredTimestamp(key)
    if (!discovered) {
      db.ref(path2).update({ [key]: timestamp() })
      log('discovered ' + key)
      logAction({
        action: 'discovered',
        target: key,
        targetName: targetData[key].name
      })
    }
  })
  return true
}

const numSearchResults = async () => {
  // console.log('count result pages')

  /* const ckRef() = uidRef().child('campaigns').child(campaignKey)

  let maxPages = await ckRef()
    .child('searchResultPageCount')
    .once('value')
    .then(getSnap)

  if (maxPages && maxPages > 0) {
    return maxPages
  } */

  // console.log('no data stored, fetching page count')

  // if no data stored, fetch manually
  let result = 1

  try {
    result = await bot.wait('.search-results__total').evaluate(() => {
      const txt = document.querySelector('.search-results__total').innerText
      const val = parseInt(txt.split(' ')[1].replace(/,/g, ''), 10)
      return val
    })
    return result
  } catch (e) {
    console.error(e)
    return 1
  }

  /*
  let result = 1
  let paginatorExists = true
  try {
    await bot
      .wait('.results-paginator')
      .exists('.results-paginator')
      .then(exists => {
        paginatorExists = exists
      })

    await bot
      .evaluate(paginatorExists => {
        if (!paginatorExists) return 1 // if only 1 page
        let num = 1

        const items = document.querySelectorAll(
          '.results-paginator button:not(.next)'
        )

        if (items.length > 0) {
          const last = items[items.length - 1]
          num = parseInt(last.innerText, 10)
        }

        return num
      }, paginatorExists)
      .then(num => {
        result = num
      })
  } catch (e) {
    console.error(e)
  }
  return result */
}

const visitRandomProfiles = async numRepeats => {
  numRepeats = await visitUnvisitedProfiles(numRepeats)
  if (numRepeats === 0) return

  console.group('visit ' + numRepeats + ' random profiles')

  const path = 'users/' + uid() + '/targetName/' + campaignKey + '/'

  await db.ref(path).once('value').then(async snapshot => {
    const data = snapshot.val() || []
    const keys = Object.keys(data)
    const count = keys.length

    let i
    if (numRepeats > count) {
      i = count
    } else {
      i = numRepeats
    }

    while (i--) {
      const randomIndex = (count * Math.random()) | 0
      const key = keys[randomIndex]
      await visitProfile({ key, data })
    }
  })

  console.groupEnd('visit ' + numRepeats + ' random profiles')
  return true
}

const getSnap = snapshot => snapshot.val()

const getFromDb = async path => db.ref(path).once('value').then(getSnap)

const getKeysFromDb = async path => {
  const data = await getFromDb(path)
  if (data) return Object.keys(data)
  return []
}

const visitUnvisitedProfiles = async numRepeats => {
  console.group('visit ' + numRepeats + ' unvisited profiles')

  const path = 'users/' + uid() + '/targetName/' + campaignKey + '/'
  const path2 = 'users/' + uid() + '/targetVisited/' + campaignKey + '/'
  const path3 = 'users/' + uid() + '/targetConnect/' + campaignKey + '/'

  const all = await getKeysFromDb(path)
  const visited = await getKeysFromDb(path2)
  const attempted = await getKeysFromDb(path3)

  // filter visited from all
  const unvisited = all.filter(tg => visited.indexOf(tg) === -1)
  const count = unvisited.length

  numRepeats = (numRepeats / 2) | 0

  let i
  if (numRepeats > count) {
    i = count
  } else {
    i = numRepeats
  }

  // filter targets allready attempted to connect
  const unconnected = visited.filter(tg => attempted.indexOf(tg) === -1)
  const count2 = unconnected.length

  let j
  if (numRepeats > count2) {
    j = count2
  } else {
    j = numRepeats
  }

  const returnNumRepeats = numRepeats * 2 - i - j

  // repeat for number of repeats
  while (i--) {
    const key = unvisited[i]
    console.log('visiting', { i, key, unvisited })
    await visitProfile({ key })
  }

  while (j--) {
    const key = unconnected[j]
    console.log('connecting', { j, key, unconnected })
    await visitProfile({ key })
  }

  console.groupEnd('visit ' + numRepeats + ' unvisited profiles')
  return returnNumRepeats
}

const isUserConnected = async () => {
  log('check if user is connected')
  let userIsConnected = false
  let exists = false
  try {
    await bot.exists('span.dist-value').then(ex => {
      exists = ex
    })

    await bot
      .evaluate(exists => {
        if (!exists) return false
        const span = document.querySelector('span.dist-value')
        return span.innerHTML === '1st'
      }, exists)
      .then(re => {
        userIsConnected = re
      })
  } catch (e) {
    console.error(e)
  }

  return userIsConnected
}

const visitProfile = async ({ key, data }) => {
  const pathv = 'users/' + uid() + '/targetVisited/' + campaignKey + '/'

  const countRef = db
    .ref('users')
    .child(uid())
    .child('campaigns')
    .child(campaignKey)
    .child('profileViews')

  const userConnected = await isUserConnected()
  const connected = await getConnectedTimestamp(key)

  if (!connected && userConnected) {
    // store undiscovered connection
    const path = 'users/' + uid() + '/targetConnected/' + campaignKey + '/'
    db.ref(path).update({ [key]: timestamp() })
    log('store new connection ' + key)
    const targetName = await getName(key)
    if (targetName) {
      logAction({
        action: 'connected',
        target: key,
        targetName
      })
    }
  }

  const visitTimestamp = await getVisitedTimestamp(key)

  const timeLimit = timestamp() - run.visit.speed * 60 * 60 // min delay

  // visit if never visited or last visit before time limit
  if (!visitTimestamp || timeLimit > visitTimestamp) {
    // search for this user
    await searchFor({
      prefix: 'https://www.linkedin.com/in/',
      data,
      key
    })

    await bot.wait(1000)

    // increment counter
    countRef.transaction(count => {
      if (!count) return 1
      count++
      return count
    })

    // scroll page
    await scrollToLoad()

    logAction({
      action: 'visited',
      target: key,
      targetName: await getName(key)
    })

    db.ref(pathv).update({ [key]: timestamp() })

    // never connect on first visit
    if (run.visit.connect && visitTimestamp) {
      if (visitTimestamp < timeLimit) {
        await tryToConnect(key)
      } else {
        log('trying to connect before time limit, abort ' + key)
      }
    }

    if (run.visit.email) {
      const email = await getEmail(key)
      if (!email) {
        await tryFetchEmail({ key })
      }
    }

    await bot.wait(random(1000, 5000))
  } else {
    log('trying to revisit before time limit, abort ' + key)
  }
}

const getText = async ({ target, desc }) => {
  log('get text: ' + desc)
  let text = ''

  try {
    await bot
      .wait(target)
      .evaluate(function (tg) {
        // now we're executing inside the browser scope.
        const node = document.querySelector(tg)
        if (node) {
          return node.innerText
        }
        return ''
      }, target)
      .then(function (re) {
        text = re
      })
  } catch (e) {
    console.error(e)
    return ''
  }

  return text
}

const tryFetchEmail = async ({ key }) => {
  await bot.scrollTo(0, 0)

  // open target sidebar
  await click({
    target: 'button.contact-see-more-less',
    desc: 'see more'
  })

  // wait sidebar to load
  await bot.wait(random(1500, 2500)).scrollTo(0, 0)

  let email = ''

  // try to fetch email
  if (await bot.exists('section.ci-email')) {
    email = await getText({
      target: 'section.ci-email > div > span',
      desc: 'email'
    })

    // if valid email
    if (email !== '') {
      // store email to db
      const path = 'users/' + uid() + '/targetEmail/' + campaignKey + '/'
      db.ref(path).update({ [key]: email })

      logAction({
        action: 'emailGet',
        target: key,
        targetName: await getName(key),
        email
      })
    }
  }

  return email
}

const scrollToLoad = async () => {
  await bot
    .wait(500) // wait for page to settle
    .scrollTo(random(200, 300), 0)
    .wait(random(200, 400))
    .scrollTo(random(400, 500), 0)
    .wait(random(200, 400))
    .scrollTo(random(600, 700), 0)
    .wait(random(200, 400))
    .scrollTo(random(800, 900), 0)
    .wait(random(200, 400))
    .scrollTo(random(1000, 1100), 0)
    .wait(random(200, 400))
    .scrollTo(random(1200, 1300), 0)
    .wait(random(1200, 1400))
}

const checkConnected = async () => {
  // check for successfull connections

  await click({
    desc: 'My Network icon',
    target: '#mynetwork-tab-icon'
  })

  await click({
    desc: 'See all link',
    target: '.mn-connections-summary__see-all'
  })

  await bot.wait(random(1000, 2000))

  if (await bot.exists('button[data-control-name=sort_by]')) {
    await click({
      desc: 'Open sort by dropdown',
      target: 'button[data-control-name=sort_by]'
    })

    await click({
      desc: 'Sort by recently added',
      target: 'button[data-control-name=sort_by_recently_added]'
    })

    const connectedCountRef = uidRef()
      .child('campaigns')
      .child(campaignKey)
      .child('connectedCount')

    const campaignKeyRef = uidRef().child('targetConnected').child(campaignKey)

    await bot
      .wait('li.connection-card')
      .wait(random(1000, 2000))
      .connectedUserIds()
      .then(async connectedIds =>
        connectedIds.forEach(async key => {
          const connected = await getConnectedTimestamp(key)
          if (!connected) {
            // update database timestamp
            campaignKeyRef.update({ [key]: timestamp() })

            connectedCountRef.transaction(count => {
              if (!count) return 1
              count++
              return count
            })
            log('connected ' + key)
            logAction({
              action: 'connected',
              target: key,
              targetName: await getName(key)
            })
          }
        })
      )
  } else {
    log('user has no recent connections')
  }
}

/* const newTarget = data => {
  data.triedToConnect = false
  data.connected = false
  data.emailed = false
  return data
} */

/*
// return new Promise((resolve, reject) => {

    // bot.cookies.clearAll() // disable any existing logins
    // bot.clearCache() // clear any tracing data
    // .on('did-stop-loading', ev => console.log(ev))

function isLoggedIn () {
  return new Promise((resolve, reject) => {
    const bot = createBot()

    bot
      .goto(LOGIN_PAGE)
      .url()
      .end()
      .then(url => {
        if (url === HOME_PAGE) {
          resolve(true)
        } else {
          resolve(false)
        }
      })
      .catch(reason => {
        reject(reason)
      })
  })
}

    bot
      .goto(LOGIN_PAGE) // .then((err, re) => console.log(err, re))
      .evaluate(function () {
        /*
        const inp = document.querySelector('#nav-typeahead-wormhole input')
        inp.value = 'John'
        inp.dispatchEvent(new Event('keyup', {'bubbles': true, 'cancelable': true}))
        const btn = document.querySelector('#nav-search-controls-wormhole button')
        btn.dispatchEvent(new Event('click', {'bubbles': true, 'cancelable': true}))

      })
      .type('#session_key-login', '') // clear email input
      .type('#session_key-login', email) // enter email
      // .type('#session_password-login', '') // clear password input
      .type('#session_password-login', password) // enter password
      .click('#btn-primary') // click sign in
      // .wait('meta[name="isGuest"]')
      .end()
      .then(() => resolve(true))
      .catch(reason => reject(reason))

    /* .wait(5000)

       .goto(SEARCH_PAGE) // .then((err, re) => console.log(err, re))
       .wait('#a11y-ember1903') // wait for search input
       .type('#a11y-ember1903', 're') // enter search query
       .click('#ember1873 > button') // click search
       .wait(5000)
*/
/*

      .evaluate(function () {
        return document.querySelector('meta[name="isGuest"]').content
      })
      .end()
      .then(isGuest => {
        if (isGuest === 'false') {
          resolve(true)
        } else {
          resolve(false)
        }
      })

    bot.end()
      .then(() => resolve(true))
      .catch(reason => reject(reason))
  })
}
*/

function checkUrl (url) {
  return new Promise((resolve, reject) => {
    const bot = createBot()

    bot
      .goto(url)
      .wait('.search-results-container')
      .evaluate(function () {
        // returns only person count on first page
        return document.querySelectorAll(
          '.search-results-container .results-list li.search-result--person'
        ).length
      })
      .then(length => {
        if (length > 0) {
          resolve(true)
        } else {
          resolve(false)
        }
      })
      .catch(reason => {
        reject(reason)
      })
  })
}

function botGetProspects (url, openedBot = false) {
  // return new Promise((resolve, reject) => {
  // const bot = createBot()

  /*
    let bot
    if (openedBot) {
      // random number from 3000 to 5000
      let randomNum = Math.floor(Math.random() * 2000) + 3000
      // random number from 1000 to 2000
      let randomNum2 = Math.floor(Math.random() * 1000) + 1000
      bot = openedBot
      bot
        .click('.results-paginator .next')
        .wait(randomNum)
        .scrollTo(1000, 1000)
        .wait(randomNum2)
        .evaluate(function () {
          let users = []
          document
            .querySelectorAll(
              '.search-result__info .search-result__result-link'
            )
            .forEach(userLink => {
              let user = {}
              user.link = userLink.href
              user.fullName = userLink
                .querySelector('.actor-name')
                .textContent.split(',')[0]
              users.push(user)
            })
          let hasNextPage
          if (document.querySelector('.results-paginator .next')) {
            hasNextPage = true
          } else {
            hasNextPage = false
          }
          let activePageElem = document.querySelector(
            '.results-paginator li.active'
          )
          let currentPage = activePageElem
            ? parseInt(activePageElem.textContent)
            : 1
          return { users, hasNextPage, currentPage }
        })
        .then(results => {
          resolve({ results, bot })
        })
        .catch(reason => {
          reject(reason)
        })
    } else {
    */
  /* global Event */
  bot
    .goto(HOME_PAGE)
    .evaluate(function () {
      const keyup = new Event('keyup', { bubbles: true, cancelable: true })
      const click = new Event('click', { bubbles: true, cancelable: true })
      let inp = document.querySelector('#nav-typeahead-wormhole input')
      inp.value = 'John'
      inp.dispatchEvent(keyup)
      var btn = document.querySelector('#nav-search-controls-wormhole button')
      btn.dispatchEvent(click)
    })
    .wait('[data-control-name=vertical_nav_people_toggle]')
    .evaluate(function () {
      const click = new Event('click', { bubbles: true, cancelable: true })
      var btn = document.querySelector(
        '[data-control-name=vertical_nav_people_toggle] button'
      )
      btn.dispatchEvent(click)
      var re = []
      for (let a of document.querySelectorAll('.search-result__info > a')) {
        const { href, innerText } = a
        re.push({ href, name: innerText })
      }
      return re
    })
  /*
        .wait('.search-results-container')
        .evaluate(function () {
          let users = []

          document
            .querySelectorAll(
              '.search-result__info .search-result__result-link'
            )
            .forEach(userLink => {
              let user = {}
              user.link = userLink.href
              user.fullName = userLink
                .querySelector('.actor-name')
                .textContent.split(',')[0]
              users.push(user)
            })
          let hasNextPage
          if (document.querySelector('.results-paginator .next')) {
            hasNextPage = true
          } else {
            hasNextPage = false
          }
          let activePageElem = document.querySelector(
            '.results-paginator li.active'
          )
          let currentPage = activePageElem
            ? parseInt(activePageElem.textContent)
            : 1
          return { users, hasNextPage, currentPage }
        })
        */
  /*
      .then(results => {
        console.log({results, bot})
        resolve({results, bot})
      })
      .catch(reason => {
        reject(reason)
      }) */
  // }
  // })
}

function botPerformAction (item, bot, message = false) {
  return new Promise((resolve, reject) => {
    bot
      .goto(item.link)
      .wait('.pv-top-card-section__name')
      .performAction[item.action](message)
      .then(status => {
        resolve(status)
      })
      .catch(reason => {
        reject(reason)
      })
  })
}

function closeBot (bot) {
  return new Promise((resolve, reject) => {
    bot
      .end()
      .then(() => {
        resolve(true)
      })
      .catch(reason => {
        reject(reason)
      })
  })
}

export {
  closeBot,
  // isLoggedIn,
  logIn,
  checkUrl,
  botGetProspects,
  botPerformAction
}
