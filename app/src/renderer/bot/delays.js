// delay options
// delay between user actions
const DEFAULT_DELAY = 24 * 60 * 60 * 1000 // 1 day to miliseconds
// delay between each action performed by bot
// const BOT_FREQUENCY = 1 * 60 * 1000 // 1 minute to miliseconds
const BOT_FREQUENCY = 15 * 1000 // 15 seconds to miliseconds

export { DEFAULT_DELAY, BOT_FREQUENCY }
