const ipcRenderer = require('electron').ipcRenderer
const ENV = process.env.NODE_ENV

export const isProduction = ENV === 'production'

export const prompt = (title, description, val) => {
  return ipcRenderer.sendSync('prompt', { title, description, val })
}

export const random = (min, max) => {
  const diff = max - min
  return min + Math.random() * diff
}

export const timestamp = () => (Date.now() / 1000) | 0
