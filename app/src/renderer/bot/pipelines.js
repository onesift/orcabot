import * as actions from './availableActions'

const defaultPipeline = [actions.VIEW, actions.CONNECT, actions.EMAIL]

export { defaultPipeline }
