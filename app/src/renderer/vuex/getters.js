export const dashboardLink = state =>
  state.auth.authenticated ? '/dashboard/' + state.auth.authenticated.uid : null
export const authenticated = state => state.auth.authenticated
export const authenticatedUid = state =>
  state.auth.authenticated ? state.auth.authenticated.uid : null
export const error = state => state.auth.error

export const welcomeTranslateKey = state => {
  switch (state.onboardingSteps.step) {
    case 0:
      return 'stepZero'
    case 1:
      return 'stepOne'
    case 2:
      return 'stepTwo'
    case 3:
      return 'stepThree'
    case 4:
      return 'stepFour'
    case 5:
      return 'stepFive'
    case 6:
      return 'stepSix'
    case 7:
      return 'stepSeven'
  }
}

export const campaignTableFields = state => {
  return {
    actions: {
      label: ''
    },
    accountName: {
      label: 'Campaign name'
      // sortable: true
    },
    prospects: {
      label: 'Prospects'
    },
    profileViews: {
      label: 'Profile Views'
    },
    connectedCount: {
      label: 'Connected'
    },
    emailsSent: {
      label: 'Emails sent'
    },
    createdAt: {
      label: 'Created'
    },
    state: {
      label: 'Status'
    }
  }
}

export const monthNames = state => {
  return [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]
}
