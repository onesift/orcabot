import * as types from './mutation-types'
import { firebase } from '../firebase'
import Tween from 'tween.js'
import { setToStorage } from '../utils'

// needed by Tween lib, called at end of this file
/* global requestAnimationFrame */
const animate = time => {
  // requestAnimationFrame(animate)
  // Tween.update(time)
}

const startTransitions = (out, currentStep, cb) => {
  let outStartRightPanel = { left: 0, opacity: 0.8 }
  let inStartRightPanel = { left: 100, opacity: 0 }

  let rightPanel = document.querySelectorAll('.container-fluid')[0]

  if (out) {
    let tween = new Tween.Tween(outStartRightPanel)
      .to({ left: 100, opacity: 0 }, 300)
      .onUpdate(() => {
        rightPanel.style.position = 'absolute'
        rightPanel.style.opacity = outStartRightPanel.opacity
        rightPanel.style.left = `-${outStartRightPanel.left}%`
      })
      .onComplete(() => {
      })
    if (typeof cb === 'function') {
      cb()
    }
    tween.start()
  } else {
    let tween = new Tween.Tween(inStartRightPanel)
      .to({ left: 0, opacity: 0.8 }, 300)
      .onUpdate(() => {
        rightPanel.style.position = 'absolute'
        rightPanel.style.opacity = inStartRightPanel.opacity
        rightPanel.style.left = `${inStartRightPanel.left}%`
      })
      .onComplete(() => {
        rightPanel.style.opacity = 1
      })
    if (typeof cb === 'function') {
      cb()
    }
    tween.start()
  }
}

export const fadeOut = ({ commit }, elem) => {
  return new Promise((resolve, reject) => {
    let startOpacity = { opacity: 0.3 }
    let tween = new Tween.Tween(startOpacity)
      .to({ opacity: 0 }, 500)
      .onUpdate(() => {
        elem.style.opacity = startOpacity.opacity
      })
      .onComplete(() => {
        resolve()
      })
    tween.start()
  })
}

export const fadeIn = ({ commit }, elem) => {
  return new Promise((resolve, reject) => {
    let startOpacity = { opacity: 0 }
    let tween = new Tween.Tween(startOpacity)
      .to({ opacity: 1 }, 500)
      .onUpdate(() => {
        elem.style.opacity = startOpacity.opacity
      })
      .onComplete(() => {
        resolve()
      })
    tween.start()
  })
}

export const fadeInElem = ({ commit }, { elem, duration }) => {
  let startOpacity = { opacity: 0.1 }
  let tween = new Tween.Tween(startOpacity)
    .to({ opacity: 1 }, 1500)
    .onUpdate(() => {
      elem.style.opacity = startOpacity.opacity
    })
    .onComplete(() => {})
  tween.start()
}

export const fadeOutElems = ({ commit }) => {
  let startOpacity = { opacity: 1 }
  let elems = document.querySelectorAll('.icons-wrapper img, .dot')
  let tween = new Tween.Tween(startOpacity)
    .to({ opacity: 0.1 }, 1000)
    .onUpdate(() => {
      elems.forEach(elem => {
        elem.style.opacity = startOpacity.opacity
      })
    })
    .onComplete(() => {})
  tween.start()
}

export const logOut = () => {
  return firebase.auth().signOut()
}

export const decrementStep = ({ commit }) => {
  commit(types.DECREMENT_ONBOARDING_STEP)
}

export const incrementStep = ({ commit, getters }) => {
  let currentStep = getters.step
  startTransitions(true, currentStep, () => {
    commit(types.INCREMENT_ONBOARDING_STEP)
    startTransitions(false, currentStep)
  })
}

export const logInUser = ({ commit }) => {
  commit(types.LOG_IN_USER)
}

export const logInLinkedIn = ({ commit }) => {
  commit('SET_STATE_FOR_LINKEDIN_LOGIN')
}

export const logOutUser = ({ commit }) => {
  logOut().then(resp => {
    commit(types.LOG_OUT_USER)
    // clear user from localStorage
    setToStorage('user', false)
  })
}

export const resetNewCampaign = ({ commit }) => {
  commit(types.RESET_LINKED_IN)
  commit(types.RESET_EMAIL)
}

animate()
