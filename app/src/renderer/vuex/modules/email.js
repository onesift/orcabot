import * as types from '../mutation-types'

const state = {
  subject: '',
  body: '',
  noEmail: false
}

const getters = {
  emailSubject: state => state.subject,
  emailBody: state => state.body,
  noEmail: state => state.noEmail,
  emailSubjectValid: state => state.subject.length > 0 || state.noEmail,
  emailBodyValid: state => state.body.length > 0 || state.noEmail
}

const actions = {
  onEmailSubjectChange: ({ commit }, event) => {
    commit(types.SET_EMAIL_SUBJECT, event.target.value)
  },
  onEmailBodyChange: ({ commit }, event) => {
    commit(types.SET_EMAIL_BODY, event.target.value)
  },
  onNoEmailChange: ({ commit }, event) => {
    commit('noEmailChange', event.target.checked)
  },
  resetEmail: ({ commit }) => {
    commit(types.RESET_EMAIL)
  }
}

const mutations = {
  [types.SET_EMAIL_SUBJECT] (state, value) {
    state.subject = value
    window.localStorage.setItem('subject', JSON.stringify(value.trim()))
  },
  [types.SET_EMAIL_BODY] (state, value) {
    state.body = value
    window.localStorage.setItem('body', JSON.stringify(value.trim()))
  },
  noEmailChange (state, value) {
    state.noEmail = value
    window.localStorage.setItem('noEmail', JSON.stringify(value))
  },
  [types.RESET_EMAIL] (state) {
    state.subject = ''
    state.body = ''
  }
}

window.state2 = state

export default {
  state,
  getters,
  actions,
  mutations
}
