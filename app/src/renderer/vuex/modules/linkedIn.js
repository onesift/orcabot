import * as types from '../mutation-types'
import { shell } from 'electron'
import { logIn } from './../../bot/bot' // isLoggedIn
// import {logIn, checkUrl} from './../../bot/bot' // isLoggedIn
import { setToStorage } from './../../utils'

// TODO: Include keytar after build succeed on windows
// const ENV = remote.process.env.NODE_ENV
// let keytar = remote.require('keytar')
// let DEBUG = false

// if (ENV === 'development' || ENV === 'testing') {
//   DEBUG = true
//   if (ENV !== 'testing') {
//     keytar = remote.require('keytar')
//   }
// } else {
//   DEBUG = false
// }

// const LINKED_IN_REGEXP = /^https:\/\/www.linkedin.com\/search\/results\/.+/

const state = {
  connected: false,
  url: '', // https://www.linkedin.com/search/results/people/?keywords=software'
  message: '',
  urlValid: false,
  urlError: false,
  verifying: false,

  error: false,
  email: '', // johnv@onesift.com',
  password: '', // orcapovio',
  credentialsValid: false
}

const getters = {
  linkedInConnected: state => state.connected,
  linkedInUrl: state => state.url,
  linkedInUrlValid: state => state.urlValid,
  linkedInUrlError: state => state.urlError,
  linkedInMessage: state => state.message,
  linkedInMessageValid: state => {
    return state.message.length > 0 && state.message.length <= 200
      ? true
      : state.message.length !== 0 ? false : null
  },
  linkedInVerifying: state => state.verifying,
  linkedInError: state => state.error,
  linkedInEmail: state => state.email,
  linkedInPassword: state => state.password,
  linkedInCredentialsValid: state => state.credentialsValid
}

const actions = {
  connectLinkedIn: ({ commit, dispatch, state }, event) => {
    let email = event.target.querySelector('input[type="email"]').value.trim()
    let password = event.target
      .querySelector('input[type="password"]')
      .value.trim()
    /* commit(types.SET_LINKED_IN_CREDENTIALS, {email, password})
    commit(types.TOGGLE_LINKED_IN_VERIFYING)
    commit(types.SET_LINKED_IN_ERROR, false)
    setTimeout(() => {
      commit(types.TOGGLE_LINKED_IN_VERIFYING)
      dispatch(
          'fadeOut',
          document.querySelector('.step-wrapper')
        ).then(() => {
          commit(types.SET_LINKED_IN_CREDENTIALS_VALID)
          dispatch(
            'fadeIn',
            document.querySelector('.step-wrapper')
          ).then(() => {
            setTimeout(() => {
              dispatch('incrementStep')
            }, 3000)
          })
        })
    }, 3000) */

    /* if (DEBUG) {
      setTimeout(() => {
        commit(types.TOGGLE_LINKED_IN_VERIFYING)
        dispatch(
          'fadeOut',
          document.querySelector('.step-wrapper')
        ).then(() => {
          commit(types.SET_LINKED_IN_CREDENTIALS, {email: '', password: ''})
          commit(types.SET_LINKED_IN_CREDENTIALS_VALID)
          dispatch(
            'fadeIn',
            document.querySelector('.step-wrapper')
          ).then(() => {
            setTimeout(() => {
              dispatch('incrementStep')
            }, 3000)
          })
        })
      }, 3000)
    } else { */
    /*
    isLoggedIn()
      .then(isLoggedIn => {
        if (isLoggedIn) {
          dispatch(
            'fadeOut',
            document.querySelector('.step-wrapper')
          ).then(() => {
            // TODO: Add keytar
            // keytar.setPassword('Orca', email, password).then(() => {
            setToStorage('linkedIn', {email: email, password: password})
            commit(types.SET_LINKED_IN_CREDENTIALS, {email: '', password: ''})
            commit(types.SET_LINKED_IN_CREDENTIALS_VALID)
            commit(types.TOGGLE_LINKED_IN_VERIFYING)
            dispatch(
              'fadeIn',
              document.querySelector('.step-wrapper')
            ).then(() => {
              setTimeout(() => {
                dispatch('incrementStep')
              }, 3000)
            })
            // })
          })
        } else {
        */
    // console.log('login')
    logIn(email, password, true, { setToStorage, dispatch, commit, types })

    /*     .then(() => {
        // console.log('login.then', status)
        // if (status) {
          // TODO: Add keytar
          // keytar.setPassword('Orca', email, password).then(() => {
/*        setToStorage('linkedIn', {email, password})
        dispatch(
            'fadeOut',
            document.querySelector('.step-wrapper')
          ).then(() => {
            /* commit(types.SET_LINKED_IN_CREDENTIALS, {
              email: '',
              password: ''
            }) */
    // dispatch('incrementStep')

    /*            commit(types.SET_LINKED_IN_CREDENTIALS_VALID)
            // commit(types.TOGGLE_LINKED_IN_VERIFYING)
            dispatch(
              'fadeIn',
              document.querySelector('.step-wrapper')
            ).then(() => {
              setTimeout(() => {
                dispatch('incrementStep')
              }, 3000)
            })
          })
      })
      .catch(() => {
        commit(types.SET_LINKED_IN_ERROR, 'Email or password are incorect!')
        commit(types.TOGGLE_LINKED_IN_VERIFYING)
      })
    /* .catch(reason => {
        commit(types.SET_LINKED_IN_ERROR, 'Email or password are incorect!')
        commit(types.TOGGLE_LINKED_IN_VERIFYING)
      }) */
    // }
  },
  openLinkedIn: ({ commit }, event) => {
    shell.openExternal('https://www.linkedin.com/search/results/people/')
  },
  onUrlChanged: ({ commit }, event) => {
    let url = event.target.value.replace(/\s+/g, '')
    commit(types.SET_LINKED_IN_URL, url)
    commit(types.TOGGLE_LINKED_IN_VERIFYING)
    if (state.url !== '') {
      commit(types.SET_LINKED_IN_URL_VALID)
      commit(types.SET_LINKED_IN_URL_ERROR, false)
      commit(types.TOGGLE_LINKED_IN_VERIFYING)
    } else {
      commit(types.SET_LINKED_IN_URL_INVALID)
      commit(types.TOGGLE_LINKED_IN_VERIFYING)
    }
    // if (LINKED_IN_REGEXP.test(url)) {
    // commit(types.SET_LINKED_IN_URL_VALID)

    // } else {
    // commit(types.SET_LINKED_IN_URL_INVALID)
    // }
  },
  checkSearchUrl: ({ commit, state, dispatch }, event) => {
    // if (state.urlValid) {
    // if (DEBUG) {
    commit(types.TOGGLE_LINKED_IN_VERIFYING)
    if (state.url !== '') {
      commit(types.SET_LINKED_IN_URL_VALID)
      commit(types.SET_LINKED_IN_URL_ERROR, false)
      commit(types.TOGGLE_LINKED_IN_VERIFYING)
      dispatch('incrementStep')
    } else {
      commit(types.SET_LINKED_IN_URL_INVALID)
      commit(types.TOGGLE_LINKED_IN_VERIFYING)
    }
    /* } else {
      commit(types.TOGGLE_LINKED_IN_VERIFYING)
      commit(types.SET_LINKED_IN_URL_ERROR, false)
      checkUrl(state.url)
        .then(valid => {
          if (valid) {
            commit(types.TOGGLE_LINKED_IN_VERIFYING)
            dispatch('incrementStep')
          } else {
            commit(
              types.SET_LINKED_IN_URL_ERROR,
              "This URL is invalid. We couldn't find any person."
            )
            commit(types.TOGGLE_LINKED_IN_VERIFYING)
          }
        })
        .catch(reason => {
          commit(types.SET_LINKED_IN_URL_ERROR, 'This URL is invalid.')
          commit(types.TOGGLE_LINKED_IN_VERIFYING)
        })
      // } */
    // }
  },
  validateLinkedInMessage: ({ commit, getters, dispatch }, event) => {
    if (getters.linkedInMessageValid) {
      dispatch('incrementStep')
    }
  },
  skipLinkedInMessage: ({ commit, dispatch }, event) => {
    commit(types.SET_LINKED_IN_MESSAGE, false)
    dispatch('incrementStep')
  },
  onMessageChanged: ({ commit }, event) => {
    commit(types.SET_LINKED_IN_MESSAGE, event.target.value)
  },
  resetLinkedIn: ({ commit }) => {
    commit(types.RESET_LINKED_IN)
  },
  goToDashboard: ({ dispatch }) => {
    dispatch('incrementStep')
    dispatch('incrementStep')
    dispatch('incrementStep')
  }
}

const mutations = {
  [types.SET_LINKED_IN_CREDENTIALS_VALID] (state) {
    state.credentialsValid = true
    state.verifying = false
  },
  [types.SET_LINKED_IN_CREDENTIALS] (state, { email, password }) {
    state.email = email
    state.password = password
  },
  [types.SET_LINKED_IN_URL_ERROR] (state, error) {
    state.urlError = error
  },
  [types.SET_LINKED_IN_ERROR] (state, error) {
    state.error = error
  },
  [types.TOGGLE_LINKED_IN_VERIFYING] (state) {
    state.verifying = !state.verifying
  },
  [types.CONNECT_LINKED_IN] (state, payload) {
    state.connected = true
  },
  [types.SET_LINKED_IN_URL] (state, value) {
    state.url = value
    window.localStorage.setItem('url', JSON.stringify(value))
  },
  [types.SET_LINKED_IN_URL_VALID] (state, payload) {
    state.urlValid = true
  },
  [types.SET_LINKED_IN_URL_INVALID] (state, payload) {
    state.urlValid = false
  },
  [types.SET_LINKED_IN_MESSAGE] (state, value) {
    if (!value) {
      value = ''
    }
    state.message = value
    window.localStorage.setItem('message', JSON.stringify(value.trim()))
  },
  [types.RESET_LINKED_IN] (state) {
    state.url = ''
    state.message = ''
    state.urlValid = false
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
