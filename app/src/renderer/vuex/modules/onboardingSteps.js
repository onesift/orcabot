import * as types from '../mutation-types'

const state = {
  step: 2
}

const getters = {
  step: state => state.step
}

const actions = {
  setStateForNewCampaign: ({ commit }) => {
    commit(types.SET_STATE_FOR_NEW_CAMPAIGN)
  },
  setStateForLinkedInLogin: ({ commit }) => {
    commit('SET_STATE_FOR_LINKEDIN_LOGIN')
  }
}

const mutations = {
  [types.DECREMENT_ONBOARDING_STEP] (state) {
    if (state.step > 0) {
      state.step--
    }
  },
  [types.INCREMENT_ONBOARDING_STEP] (state) {
    if (state.step < 6) {
      state.step++
    }
  },
  [types.SET_STATE_FOR_NEW_CAMPAIGN] (state) {
    state.step = 4
  },

  SET_STATE_FOR_LINKEDIN_LOGIN (state) {
    state.step = 1
  }
}

window.state6 = {
  state,
  actions,
  mutations
}

export default {
  state,
  getters,
  actions,
  mutations
}
