import { firebaseAction, firebaseMutations } from 'vuexfire'
import * as types from '../mutation-types'
import router from '../../router'
import { getFromStorage, setToStorage } from '../../utils'
import queryString from 'query-string'
import { addToQueue, getFromQueue, clearQueue } from '../../bot/queue'
import { defaultPipeline } from '../../bot/pipelines'
import { closeBot, botPerformAction } from '../../bot/bot'
import createBot from '../../bot/botInit'
import * as DELAYS from '../../bot/delays'
import * as LIMITS from '../../bot/dailyLimits'

// this is reference to user/campaigns in firebase
// will be defined once Dasboard component is loaded
let userCampaignsRef
// this is reference to last campaign created from steps
// so we can start it if called with 'START CAMPAIGN'
let lastCampaign
// we need to count campaigns on each change
// so we can update campaign account name with CAMPAIGN ${N}
let campaignsCount = 0
// we are storing reference to running campaign from firebase
let runningCampaignRef
// we need reference to bot interval defined with setInterval
// so we can clear it on campaign pause, or forced pause
let botPerformInterval
let botInterval
let performBOT
let searchBOT

const state = {
  // firebase campaigns
  campaigns: [],
  // when START CAMPAIGN is called, when some campaign is already running
  // this will handle that error
  startError: false,
  // this is for future realeases when we add payment plans
  // right now it is used to disable 'NEW CAMPAIGN' button
  paymentPlan: false,
  // we need to know if some bot is running
  // and store the reference to that bot
  // so we can turn them of before user close the app or pause the campaign
  // to free the memory
  botRunning: false
}

const getters = {
  campaigns: state => state.campaigns.filter(c => c.accountName), // hide empty
  hasCampaignRunning: state => {
    return state.campaigns.some(camp => camp.state === 'running')
  },
  runningCampaing: state => {
    return state.campaigns.find(camp => camp.state === 'running')
  },
  startError: state => state.startError,
  paymentPlan: state => state.paymentPlan,
  botRunning: state => state.botRunning,
  forcedPausedCampaign: state => {
    return state.campaigns.find(camp => camp.forcedPause === true)
  }
}

const actions = {
  setUserCampaignsRef: firebaseAction(
    ({ bindFirebaseRef, unbindFirebaseRef }, { ref }) => {
      bindFirebaseRef('campaigns', ref)
      userCampaignsRef = ref
      userCampaignsRef.on('value', snap => {
        campaignsCount = snap.numChildren()
        if (!campaignsCount) {
          campaignsCount = 0
        }
      })
    }
  ),
  redirectAndCreateCampaign: ({ commit, getters }) => {
    if (getters.emailSubjectValid && getters.emailBodyValid) {
      commit(types.SET_START_ERROR, false)
      router.push({
        name: 'dashboard',
        params: { uid: getters.authenticatedUid },
        query: { create: true, start: false }
      })
    }
  },
  goToDashboard: ({ commit, getters }) => {
    commit(types.SET_START_ERROR, false)
    router.push({
      name: 'dashboard',
      params: { uid: getters.authenticatedUid },
      query: { create: false, start: false }
    })
  },
  redirectAndCreateAndStartCampaign: ({ commit, getters }) => {
    if (getters.emailSubjectValid && getters.emailBodyValid) {
      if (!getters.hasCampaignRunning) {
        commit(types.SET_START_ERROR, false)
        router.push({
          name: 'dashboard',
          params: { uid: getters.authenticatedUid },
          query: { create: true, start: true }
        })
      } else {
        commit(
          types.SET_START_ERROR,
          'You already have one campaign running. Please click to run campaign latter.'
        )
      }
    }
  },
  createCampaignFromState: ({ getters, commit, state }) => {
    // prevent bug creating a new campaign on start
    if (window.appStart > Date.now() - 5000) return

    let accountName
    let parsed = queryString.parse(getters.linkedInUrl.split('?')[1])
    if (parsed.keywords) {
      accountName = campaignsCount + 1 + ' ' + parsed.keywords
    } else {
      accountName = `CAMPAIGN ${campaignsCount + 1}`
    }
    let newCampaign = {
      url: getters.linkedInUrl,
      message: getters.linkedInMessage,
      subject: getters.emailSubject,
      body: getters.emailBody,
      state: 'paused',
      accountName: accountName,
      prospects: 0,
      prospectsParsed: false,
      prospectsParsedLast: false,
      profileViews: 0,
      delay: DELAYS.DEFAULT_DELAY,
      connectedCount: 0,
      emailsSent: 0,
      createdAt: Date.now(),
      pipeline: defaultPipeline,
      forcedPause: false,
      users: [],
      noEmail: getters.noEmail
    }
    commit(types.CREATE_CAMPAIGN, newCampaign)
    let user = getFromStorage('user')
    user.hasCampaigns = true
    setToStorage('user', user)
  },
  startLastCampaign: ({ commit, dispatch }) => {
    if (lastCampaign) {
      dispatch('startCampaign', lastCampaign.getKey())
    }
  },
  forceStart: ({ commit }, campaignKey) => {
    commit(types.FORCE_START_CAMPAIGN, campaignKey)
  },
  startProcessingQueue: ({ dispatch, commit, getters }) => {
    dispatch('putRunningCampaignIntoQUEUE').then(() => {
      let bot = createBot()
      performBOT = bot
      commit(types.SET_BOT_RUNNING, true)
      // set how frequently we will check QUEUE for new actions
      botPerformInterval = setInterval(() => {
        // we are using try/catch to handle error from getFromQueue
        // TODO: Change this to promise maybe?
        try {
          let item = getFromQueue()
          let campaign = getters.runningCampaing
          console.log(campaign.message)
          botPerformAction(item, bot, campaign.message)
            .then(resp => {
              if (resp.status) {
                commit(types.SET_ACTION_PROCESSED, { item, campaign })
              } else {
                commit(types.SET_ACTION_DERMANT, item)
              }
              dispatch('removeFromQUEUE', item.key)
            })
            .catch(reason => {
              console.log(reason)
              // something wrong in BOT,
              // we will add again this action into internal QUEUE
              // QUEUE from firebase already has this item
              addToQueue(item)
            })
        } catch (error) {
          // here we cathing error that can be trown from getFromQueue() when QUEUE is empty
          console.log(error.message)
        }
        // BOT_FREQUENCY is delay between performing actions with bot
      }, DELAYS.BOT_FREQUENCY)
    })
  },
  parseSearchUsers: ({ commit }, { campaign, campaignKey }) => {
    /*
    let campUrl = campaign.prospectsParsedLast ? campaign.url + '&page=' + (parseInt(campaign.prospectsParsedLast) + 1) : campaign.url
    botGetProspects(campUrl).then(({ results, bot }) => {
      commit(types.SET_BOT_RUNNING, true)
      let { users, hasNextPage, currentPage } = results
      searchBOT = bot
      if (hasNextPage) {
        commit(types.UPDATE_PROSPECTS, { users, currentPage, hasNextPage, campaignKey })
        let openedBot = bot
        botInterval = setInterval(() => {
          botGetProspects(campaign.url, openedBot).then((newResults) => {
            openedBot = newResults.bot
            searchBOT = newResults.bot
            commit(types.UPDATE_PROSPECTS, {
              users: newResults.results.users,
              currentPage: newResults.results.currentPage,
              hasNextPage: newResults.results.hasNextPage,
              campaignKey
            })
            if (!newResults.results.hasNextPage) {
              closeBot(openedBot).then(status => {
                searchBOT = null
                commit(types.SET_BOT_RUNNING, false)
                clearInterval(botInterval)
              })
            }
          })
        }, 10000)
      } else {
        commit(types.UPDATE_PROSPECTS, { users: results.users, currentPage: results.currentPage, hasNextPage: results.hasNextPage, campaignKey })
        closeBot(bot).then(status => {
          searchBOT = null
          commit(types.SET_BOT_RUNNING, false)
        })
      }
    }) */
  },
  startCampaign: ({ commit, getters, dispatch }, campaignKey) => {
    if (!getters.hasCampaignRunning) {
      commit(types.START_CAMPAIGN, campaignKey)

      let campaign = getters.campaigns.find(camp => {
        return camp['.key'] === campaignKey
      })

      window.localStorage.setItem('message', JSON.stringify(campaign.message))
      window.localStorage.setItem('subject', JSON.stringify(campaign.subject))
      window.localStorage.setItem('body', JSON.stringify(campaign.body))
      window.localStorage.setItem('url', JSON.stringify(campaign.url))
      window.localStorage.setItem('campaignKey', JSON.stringify(campaignKey))

      const loop = _ => {
        // if (window.botActive) return // abort if bot is active
        // disable bot starting the old way
        // logIn('', '', false)
      }

      botInterval = setInterval(loop, 1000 * 20) // check every 20s
      loop()

      /*
      if (campaign.prospectsParsed) {
        // search users aka prospects are parsed so we can process them
        dispatch('startProcessingQueue')
      } else {
        // search users are not parsed, parse them page by page
        dispatch('parseSearchUsers', { campaign, campaignKey })
        // start perform bot also after 1 minute as we will have some users to process
        setTimeout(() => {
          // check if same campaign is still running
          if (getters.runningCampaing && getters.runningCampaing['.key'] === campaignKey) {
            dispatch('startProcessingQueue')
          }
        }, DELAYS.BOT_FREQUENCY)
    } */
    }
  },
  clearQUEUE: () => {
    runningCampaignRef.child('IN_QUEUE').set([])
  },
  clearInternalQUEUE: () => {
    clearQueue()
  },
  removeFromQUEUE: (context, key) => {
    runningCampaignRef.child(`IN_QUEUE/${key}`).remove()
  },
  updateUserDermant: (context, userKey) => {
    runningCampaignRef.child(`users/${userKey}`).update({
      dermant: true
    })
  },
  putRunningCampaignIntoQUEUE: ({ dispatch, getters }) => {
    if (runningCampaignRef) {
      runningCampaignRef.child('IN_QUEUE').once('value', data => {
        let campaign = getters.runningCampaing
        let alreadyInQueue = data.val()
        if (alreadyInQueue) {
          dispatch('countinueProcessingItems', { campaign, alreadyInQueue })
        } else {
          dispatch('addNewItemsIntoQueue', campaign.pipeline)
        }
      })
    }
  },
  countinueProcessingItems: ({ dispatch }, { campaign, alreadyInQueue }) => {
    // countinue proccessing items from QUEUE
    let keys = Object.keys(alreadyInQueue)
    keys.forEach(key => {
      let item = alreadyInQueue[key]
      // if added before 24h set as dermant and remove it from firebase IN_QUEUE
      if (item.addedAt + campaign.delay < Date.now()) {
        dispatch('updateUserDermant', item.userKey).then(() => {
          dispatch('removeFromQUEUE', key)
        })
      } else {
        item.key = key
        addToQueue(item)
      }
    })
  },
  addNewItemsIntoQueue: ({ commit }, pipeline) => {
    // add new items to QUEUE as it is empty
    pipeline.forEach(action => {
      let dailyLimit = LIMITS[action.toUpperCase()]
      runningCampaignRef
        .child('users')
        .orderByChild('nextAction')
        .equalTo(action)
        .limitToFirst(dailyLimit)
        .once('value', data => {
          let response = data.val()
          if (response) {
            let keys = Object.keys(response)
            // TODO: check if delay has passed here
            keys.forEach(key => {
              let item = {
                action: action,
                userKey: key,
                link: response[key].link,
                addedAt: Date.now()
              }
              let itemInQueue = runningCampaignRef.child('IN_QUEUE').push(item)
              item.key = itemInQueue['.key']
              addToQueue(item)
            })
          }
        })
    })
  },
  runningCampaign: ({ state }) => {
    return state.campaigns.find(camp => camp.state === 'running')
  },
  forcePauseRunningCampaign: ({ dispatch, commit, getters }) => {
    clearTimeout(window.localStorage.getItem('autorun'))
    if (botInterval) {
      clearInterval(botInterval)
    }
    if (botPerformInterval) {
      clearInterval(botPerformInterval)
    }
    dispatch('runningCampaign').then(campaign => {
      dispatch('clearInternalQUEUE').then(() => {
        commit(types.FORCE_PAUSE_RUNNING_CAMPAIGN, campaign['.key'])
        if (getters.botRunning) {
          if (performBOT) {
            closeBot(performBOT).then(status => {
              performBOT = null
            })
          }
          if (searchBOT) {
            closeBot(searchBOT).then(status => {
              searchBOT = null
            })
          }
          commit(types.SET_BOT_RUNNING, false)
        }
      })
    })
  },
  pauseCampaign: ({ dispatch, commit, getters }, campaignKey) => {
    if (window.botActive) {
      /* global alert */
      alert('Can not pause while bot is active.')
      return
    }

    clearTimeout(window.localStorage.getItem('autorun'))
    if (botInterval) {
      clearInterval(botInterval)
      botInterval = 0
    }

    if (botPerformInterval) {
      clearInterval(botPerformInterval)
      botPerformInterval = 0
    }

    dispatch('clearInternalQUEUE').then(() => {
      commit(types.PAUSE_CAMPAIGN, campaignKey)
      if (getters.botRunning) {
        if (performBOT) {
          closeBot(performBOT).then(status => {
            performBOT = null
          })
        }
        if (searchBOT) {
          closeBot(searchBOT).then(status => {
            searchBOT = null
          })
        }
        commit(types.SET_BOT_RUNNING, false)
      }
    })
  }
}

const mutations = {
  [types.SET_ACTION_DERMANT] (state, item) {
    runningCampaignRef.child(`users/${item.userKey}`).update({
      currentAction: item.action,
      dermant: true
    })
  },
  [types.SET_ACTION_PROCESSED] (state, { item, campaign }) {
    let nextAction
    let currentActionIndex = campaign.pipeline.indexOf(item.action)
    if (campaign.pipeline.length - 1 === currentActionIndex) {
      nextAction = 'done'
    } else {
      nextAction = campaign.pipeline[currentActionIndex + 1]
    }
    let firebaseKey = {
      view: 'profileViews',
      connect: 'connectedCount',
      email: 'emailsSent'
    }[item.action]
    runningCampaignRef
      .child(`${firebaseKey}`)
      .set(campaign[`${firebaseKey}`] + 1)
    runningCampaignRef.child(`users/${item.userKey}`).update({
      currentAction: item.action,
      currentActionProcessedAt: Date.now(),
      nextAction: nextAction
    })
  },
  [types.SET_BOT_RUNNING] (state, value) {
    state.botRunning = value
  },
  [types.UPDATE_PROSPECTS] (state, payload) {
    let campaign = state.campaigns.find(
      campaign => campaign['.key'] === payload.campaignKey
    )
    payload.users.forEach(user => {
      user.currentAction = 'start'
      user.nextAction = defaultPipeline[0]
      user.dermant = false
      user.currentActionStartedAt = Date.now()
      runningCampaignRef.child('users').push(user)
    })
    runningCampaignRef.update({
      prospects: campaign.prospects + payload.users.length,
      prospectsParsedLast: payload.currentPage
    })
    if (!payload.hasNextPage) {
      runningCampaignRef.update({
        prospectsParsed: true
      })
    }
  },
  [types.CREATE_CAMPAIGN] (state, newCampaign) {
    lastCampaign = userCampaignsRef.push(newCampaign)
  },
  [types.START_CAMPAIGN] (state, campaignKey) {
    userCampaignsRef.child(campaignKey).update({
      state: 'running'
    })
    runningCampaignRef = userCampaignsRef.child(campaignKey)
  },
  [types.SET_START_ERROR] (state, error) {
    state.startError = error
  },
  [types.PAUSE_CAMPAIGN] (state, campaignKey) {
    userCampaignsRef.child(campaignKey).update({
      state: 'paused'
    })
    runningCampaignRef = null
  },
  [types.FORCE_PAUSE_RUNNING_CAMPAIGN] (state, campaignKey) {
    userCampaignsRef.child(campaignKey).update({
      state: 'paused',
      forcedPause: true
    })
    runningCampaignRef = null
  },
  [types.FORCE_START_CAMPAIGN] (state, campaignKey) {
    userCampaignsRef.child(campaignKey).update({
      forcedPause: false
    })
  },
  [types.COMPLETE_CAMPAIGN] (state, campaignKey) {
    userCampaignsRef.child(campaignKey).update({
      state: 'completed'
    })
  },
  ...firebaseMutations
}

export default {
  state,
  getters,
  mutations,
  actions
}
