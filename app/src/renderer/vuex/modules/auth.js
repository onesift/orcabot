import * as types from '../mutation-types'
import { firebase, db } from '../../firebase'
import { getFromStorage, setToStorage } from '../../utils'
import { parse } from 'url'
// import { remote, shell } from 'electron'
import { remote } from 'electron'
import axios from 'axios'
import qs from 'qs'
import google from 'googleapis'
// import Base64 from 'js-base64'
// const ENV = remote.process.env.NODE_ENV

// let DEBUG = true
// if (ENV === 'development' || ENV === 'testing') {
//   DEBUG = true
// } else {
//   DEBUG = false
// }

// const GOOGLE_AUTHORIZATION_URL = 'https://accounts.google.com/o/oauth2/v2/auth'
const GOOGLE_TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token'
const GOOGLE_VALIDATE_TOKEN_URL =
  'https://www.googleapis.com/oauth2/v3/tokeninfo'
const GOOGLE_PROFILE_URL = 'https://www.googleapis.com/userinfo/v2/me'
const GOOGLE_REDIRECT_URI = 'http://127.0.0.1:9080'
const GOOGLE_CLIENT_SECRET = '_qjzlSYiMwM-kHVUfbOjpjc9'
const GOOGLE_CLIENT_ID =
  '866795017632-q7m5hf727lbnrroo473v3al0609l4rhn.apps.googleusercontent.com'
const GMAIL_URL = 'https://www.googleapis.com/gmail/v1/users'

const OAuth2 = google.auth.OAuth2
const oauth2Client = new OAuth2(
  GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET,
  GOOGLE_REDIRECT_URI
)

// log last login
const logLogin = async () => {
  try {
    return window.db
      .ref('users')
      .child(window.firebase.auth().currentUser.uid)
      .child('lastActive')
      .set(new Date().valueOf())
  } catch (err) {
    return console.log('Log has no connection to Firebase: ' + err)
  }
}

function signInWithPopup () {
  google.options({
    auth: oauth2Client // set auth as a global default
  })

  return new Promise((resolve, reject) => {
    const authWindow = new remote.BrowserWindow({
      width: 800,
      height: 600,
      show: true
    })

    // TODO: Generate and validate PKCE code_challenge value
    /* const urlParams = {
      response_type: 'code',
      redirect_uri: GOOGLE_REDIRECT_URI,
      client_id: GOOGLE_CLIENT_ID,
      scope: 'profile email https://www.googleapis.com/auth/gmail.readonly'
      scope: 'profile email https://mail.google.com/'
    } */

    // const authUrl = `${GOOGLE_AUTHORIZATION_URL}?${qs.stringify(urlParams)}`

    function handleNavigation (url) {
      const query = parse(url, true).query

      if (query) {
        if (query.error) {
          reject(new Error('Something went wrong. Please try again.'))
        } else if (query.code) {
          // Login is complete
          authWindow.removeAllListeners('closed')
          setImmediate(() => authWindow.close())

          // This is the authorization code we need to request tokens
          resolve(query.code)
        }
      }
    }

    authWindow.on('closed', () => {
      reject(new Error('Something went wrong. Please try again.'))
    })

    authWindow.webContents.on('will-navigate', (event, url) =>
      handleNavigation(url)
    )

    authWindow.webContents.on(
      'did-get-redirect-request',
      (event, oldUrl, newUrl) => handleNavigation(newUrl)
    )

    const scope = ['profile', 'email', 'https://mail.google.com/']

    const authUrl = oauth2Client.generateAuthUrl({ scope })
    authWindow.loadURL(authUrl + '&approval_prompt=force')
  })
}

async function fetchAccessTokens (code) {
  const response = await axios.post(
    GOOGLE_TOKEN_URL,
    qs.stringify({
      code,
      client_secret: GOOGLE_CLIENT_SECRET,
      client_id: GOOGLE_CLIENT_ID,
      redirect_uri: GOOGLE_REDIRECT_URI,
      grant_type: 'authorization_code'
    }),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
  )
  return response.data
}

async function fetchGoogleProfile (accessToken) {
  const response = await axios.get(GOOGLE_PROFILE_URL, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`
    }
  })
  return response.data
}

async function validateAccessToken () {
  const user = getFromStorage('providerUser')
  const params = qs.stringify({
    access_token: user.tokens.access_token
  })
  const url = `${GOOGLE_VALIDATE_TOKEN_URL}?${params}`
  const response = await axios.get(url, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return response.data
}

async function fetchAllEmails () {
  const user = getFromStorage('providerUser')
  const url = `${GMAIL_URL}/${user.uid}/messages`
  const response = await axios.get(url, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${user.tokens.access_token}`
    }
  })
  return response.data
}

/* function isUserEqual (googleUser, firebaseUser) {
  if (firebaseUser) {
    let providerData = firebaseUser.providerData
    for (let i = 0; i < providerData.length; i++) {
      if (
        providerData[i].providerId ===
          firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
        providerData[i].uid === googleUser.uid
      ) {
        return true
      }
    }
  }
  return false
} */

function onSignIn (googleUser, callback) {
  let unsubscribe = firebase.auth().onAuthStateChanged(
    firebaseUser => {
      unsubscribe()
      // if (!isUserEqual(googleUser, firebaseUser)) {
      const credential = firebase.auth.GoogleAuthProvider.credential(
        googleUser.idToken
      )
      firebase
        .auth()
        .signInWithCredential(credential)
        .then(function (resp) {
          let user = firebase.auth().currentUser
          user.hasCampaigns = false
          setToStorage('user', user)
          db
            .ref('users/' + user.uid)
            .update({ name: user.displayName, email: user.email })
          callback(null, { status: true, user })
        })
        .catch(function (error) {
          callback(new Error(error), { status: false, error: error })
        })
      /* } else {
        callback(null, {status: true, user: firebase.auth().currentUser})
      } */
    },
    () => {
      callback(new Error('Something went wrong. Please try again.'), {
        status: false,
        error: 'Something went wrong. Please try again.'
      })
    }
  )
}

const state = {
  authenticated: getFromStorage('user'),
  verifying: false,
  connected: false,
  error: false
}

const mutations = {
  [types.SET_GOOGLE_ERROR] (state, error) {
    state.error = error
  },
  [types.SET_GOOGLE_CONNECTED] (state) {
    state.connected = true
    state.verifying = false
    // shell.openExternal('https://orca-31b91.firebaseapp.com')

    // if (process.platform === 'darwin') {
    //   child_process.execSync('open http://example.com') // mac
    // } else {
    //   child_process.execSync('start http://example.com') // win
    // }
  },
  [types.TOGGLE_GOOGLE_VERIFYING] (state) {
    state.verifying = !state.verifying
  },
  [types.LOG_IN_USER] (state, payload) {
    state.authenticated = payload.user
  },
  [types.LOG_OUT_USER] (state) {
    state.authenticated = false
  },
  [types.ON_LOG_IN_FAILED] (state, error) {
    state.authenticated = false
    state.error = error
  }
}

const getters = {
  googleVerifying: state => state.verifying,
  googleConnected: state => state.connected,
  googleError: state => state.error
}

const googleOnSignInCallback = (err, re, commit, dispatch) => {
  if (err) {
    commit(types.SET_GOOGLE_ERROR, err)
    commit(types.TOGGLE_GOOGLE_VERIFYING)
    return
  }

  commit(types.LOG_IN_USER, { user: re.user })
  commit(types.TOGGLE_GOOGLE_VERIFYING)
  dispatch('fadeOut', document.querySelector('.step-wrapper')).then(() => {
    commit(types.SET_GOOGLE_CONNECTED)
    dispatch('fadeIn', document.querySelector('.step-wrapper')).then(() => {
      setTimeout(() => {
        dispatch('incrementStep')
      }, 3000)
    })
  })
}

const afterSignInWithPopup = (code, commit, dispatch) => {
  fetchAccessTokens(code).then(tokens => {
    // oauth2Client.getToken(query.code, (err, tokens) => {
    // Now tokens contains an access_token
    // and an optional refresh_token. Save them.
    // if (!err) {
    console.log(tokens)
    oauth2Client.setCredentials(tokens)
    // } else {
    // console.error(err)
    // }
    // })

    fetchGoogleProfile(tokens.access_token).then(resp => {
      const providerUser = {
        uid: resp.id,
        email: resp.email,
        tokens,
        displayName: resp.name,
        idToken: tokens.id_token
      }
      setToStorage('providerUser', providerUser)
      onSignIn(providerUser, (err, re) =>
        googleOnSignInCallback(err, re, commit, dispatch)
      )
      logLogin()
      setTimeout(_ => window.location.reload(), 3000)
    })
  })
}

const actions = {
  validateToken: async ({ commit }, event) => {
    const response = await validateAccessToken()
    console.log(response)
  },
  allEmails: async ({ commit }, event) => {
    const response = await fetchAllEmails()
    console.log(response)
  },
  googleSignIn: async ({ commit, dispatch }) => {
    commit(types.TOGGLE_GOOGLE_VERIFYING)
    commit(types.SET_GOOGLE_ERROR, false)
    // if (!DEBUG) {
    signInWithPopup()
      .then(code => afterSignInWithPopup(code, commit, dispatch))
      .catch(reason => {
        commit(types.TOGGLE_GOOGLE_VERIFYING)
        commit(types.SET_GOOGLE_ERROR, reason)
      })
    /* } else {
      setTimeout(() => {
        commit(types.TOGGLE_GOOGLE_VERIFYING)
        dispatch(
          'fadeOut',
          document.querySelector('.step-wrapper')
        ).then(() => {
          commit(types.SET_GOOGLE_CONNECTED)
          dispatch(
            'fadeIn',
            document.querySelector('.step-wrapper')
          ).then(() => {
            setTimeout(() => {
              dispatch('incrementStep')
            }, 3000)
          })
        })
      }, 3000) */
    // }
  }
}

window.state4 = state

export default {
  state,
  mutations,
  actions,
  getters
}
