import Vue from 'vue'
import Vuex from 'vuex'
import vuexI18n from 'vuex-i18n'
import * as actions from './actions'
import * as getters from './getters'
import modules from './modules'
import * as translations from './../translations'
import router from './../router'
import { sync } from 'vuex-router-sync'

Vue.use(Vuex)

modules['i18n'] = vuexI18n.store

const store = new Vuex.Store({
  actions,
  getters,
  modules,
  strict: false
})

Vue.use(vuexI18n.plugin, store)

// add translations
Vue.i18n.add('en', translations.EN)
Vue.i18n.add('fr', translations.FR)

// set default locale to en
Vue.i18n.set('en')

sync(store, router)

export default store
