export const getFromStorage = key => {
  return JSON.parse(localStorage.getItem(key))
}

export const setToStorage = (key, object) => {
  localStorage.setItem(key, JSON.stringify(object))
}

export const getActionMessage = ({ action, targetName }) => {
  const messages = {
    start: 'Orca started diving...',
    emailSent: `Orca sent an email to ${targetName}.`,
    connect: `Orca sent an invitation to ${targetName}.`,
    discovered: `Orca found a new person: ${targetName}.`,
    connected: `Orca connected to ${targetName}.`,
    visited: `Orca got an email from ${targetName}.`
  }

  return messages[action] || `Orca performed an action called ${action}.`
}
