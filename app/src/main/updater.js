'use strict'

import simpleUpdater from 'electron-simple-updater'
import log from 'electron-log'

const ENV = process.env.NODE_ENV
const DEBUG = false
const UPDATES_URL =
  'https://s3-us-west-2.amazonaws.com/orca-updates/updates.json'
const isProduction = ENV === 'production'
const updatesEnabled = isProduction || DEBUG

/**
 * According to electron-log package specs, logs can be found under a location:
 * on Linux: ~/.config/<app name>/log.log
 * on OS X: ~/Library/Logs/<app name>/log.log
 * on Windows: %USERPROFILE%\AppData\Roaming\<app name>\log.log
 *
 * Updater is turned on only at the production just to not auto update the app
 * while developing and when there is a new version available.
 *
 * Updater can be turned on at the development by manually changing
 * DEBUG to true.
 *
 */

const debug = () => {
  if (updatesEnabled) {
    log.info('Updater debug starting...')

    simpleUpdater.on('checking-for-update', () =>
      log.info('Updater checking for updates...')
    )

    simpleUpdater.on('update-available', meta => {
      log.info('Updater update available:', meta)
    })

    simpleUpdater.on('update-not-available', () => {
      log.info('Updater update not available')
    })

    simpleUpdater.on('update-downloading', meta => {
      log.info('Updater update downloading:', meta)
    })

    simpleUpdater.on('update-downloaded', () => {
      log.info('Updater update downloaded')
    })

    simpleUpdater.on('squirrel-win-installer', ev => {
      log.info('Updater squirrel-win-installer:', ev)
    })

    simpleUpdater.on('error', err => log.error('Updater error:', err))
  }
}

const init = () => {
  simpleUpdater.logger = log
  simpleUpdater.logger.transports.file.level = 'info'

  simpleUpdater.setOptions('disabled', !updatesEnabled) // we need to change it to true if we want publish app to the App Store or to the Windows Store

  simpleUpdater.init(UPDATES_URL)

  if (updatesEnabled) {
    log.info('Updater initialization...')
    log.info(`Updater current build: ${simpleUpdater.build}`)
    log.info(`Updater current buildId: ${simpleUpdater.buildId}`)
    log.info(`Updater current channel: ${simpleUpdater.channel}`)
    log.info(`Updater current app version: ${simpleUpdater.version}`)
  }

  simpleUpdater.on('update-downloaded', meta => {
    simpleUpdater.quitAndInstall()
  })
}

const updater = {
  debug,
  init
}

export default updater
