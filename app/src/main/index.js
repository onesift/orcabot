'use strict'

import {
  app,
  BrowserWindow,
  powerSaveBlocker,
  Menu,
  session,
  ipcMain,
  Tray
} from 'electron'
import path from 'path'

import randomUa from 'random-useragent'
import log from 'electron-log'
import updater from './updater'
import prompt from './prompt'
import autolauncher from './autolauncher'

const ENV = process.env.NODE_ENV
const isProduction = ENV === 'production'

let tray
let mainWindow

const assetsDirectory = path.normalize(
  path.join(__dirname, 'icons')
)

const createTray = () => {
  tray = new Tray(path.join(assetsDirectory, 'TrayIconTemplate.png'))
  tray.on('right-click', toggleWindow)
  tray.on('double-click', toggleWindow)
  tray.on('click', toggleWindow)
}

const getWindowPosition = () => {
  const windowBounds = mainWindow.getBounds()
  const trayBounds = tray.getBounds()

  const x = Math.round(
    trayBounds.x + (trayBounds.width / 2) - (windowBounds.width / 2)
  )

  const y = Math.round(
    trayBounds.y + trayBounds.height + 4
  )

  return { x, y }
}

const BROWSER_INFO = randomUa.getRandomData()

const winURL =
  process.env.NODE_ENV === 'development'
    ? `http://localhost:${require('../../../config').port}`
    : `file://${__dirname}/index.html`

const setHeaders = () => {
  session.defaultSession.webRequest.onBeforeSendHeaders((details, callback) => {
    details.requestHeaders['User-Agent'] = BROWSER_INFO.userAgent
    // eslint-disable-next-line
    callback({
      cancel: false,
      requestHeaders: details.requestHeaders
    })
  })
}

const createWindow = () => {
  mainWindow = new BrowserWindow({
    title: 'Orca',
    width: 400,
    height: 400,
    show: false,
    frame: false,
    fullscreenable: false,
    resizable: false,
    transparent: true,
    'node-integration': false
  })

  mainWindow.loadURL(winURL)

  if (!isProduction) {
    mainWindow.openDevTools({mode: 'detach'})
  }

  mainWindow.on('blur', () => mainWindow.hide())

  mainWindow.on('closed', () => {
    mainWindow = null
    app.quit()
  })

  powerSaveBlocker.start('prevent-app-suspension')

  // Create the Application's main menu
  const template = [
    {
      label: 'Application',
      submenu: [
        {
          label: 'Quit',
          accelerator: 'Command+Q',
          click: function () {
            app.quit()
          }
        }
      ]
    },
    {
      label: 'Edit',
      submenu: [
        { label: 'Undo', accelerator: 'CmdOrCtrl+Z', selector: 'undo:' },
        { label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:' },
        { type: 'separator' },
        { label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:' },
        { label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:' },
        { label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:' },
        {
          label: 'Select All',
          accelerator: 'CmdOrCtrl+A',
          selector: 'selectAll:'
        }
      ]
    },
    {
      label: 'View',
      submenu: [
        { role: 'reload' },
        { role: 'forcereload' },
        { role: 'toggledevtools' },
        { type: 'separator' },
        { role: 'resetzoom' },
        { role: 'zoomin' },
        { role: 'zoomout' },
        { type: 'separator' },
        { role: 'togglefullscreen' }
      ]
    }
  ]

  Menu.setApplicationMenu(Menu.buildFromTemplate(template))
}

const toggleWindow = () => {
  if (mainWindow.isVisible()) {
    mainWindow.hide()
  } else {
    showWindow()
  }
}

const showWindow = () => {
  const position = getWindowPosition()
  mainWindow.setPosition(position.x, position.y, false)
  mainWindow.show()
  mainWindow.focus()
}

ipcMain.on('show-window', () => {
  showWindow()
})

app.on('ready', () => {
  updater.debug()
  updater.init()
  app.setName(BROWSER_INFO.browserName)
  prompt.init()
  autolauncher.init()
  setHeaders()
  createWindow()
  createTray()
  showWindow()
  log.info(`Current app version: ${app.getVersion()}`)
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
    app.dock.hide()
  }
})
