'use strict'

import AutoLaunch from 'auto-launch'
import log from 'electron-log'

const ENV = process.env.NODE_ENV
const DEBUG = false
const isProduction = ENV === 'production'
const autolauncherEnabled = isProduction || DEBUG

/**
 * Autolauncher can be turned on at the development by manually changing
 * DEBUG to true.
 *
 */
const init = () => {
  const autoLauncher = new AutoLaunch({
    name: 'Orca'
  })

  if (autolauncherEnabled) {
    autoLauncher
      .isEnabled()
      .then(function (isEnabled) {
        if (isEnabled) {
          return null
        }
        autoLauncher.enable()
      })
      .catch(function (err) {
        log.error(`AutoLauncher error: ${err}`)
      })
  }
}

const autolauncher = {
  init
}

export default autolauncher
