'use strict'

import { BrowserWindow, ipcMain } from 'electron'

let promptResponse

const init = () => {
  ipcMain.on('prompt', function (eventRet, arg) {
    promptResponse = null
    let promptWindow = new BrowserWindow({
      width: 500,
      height: 360,
      show: false,
      resizable: false,
      movable: false,
      alwaysOnTop: true,
      frame: false,
      center: true,
      modal: true
    })
    arg.val = arg.val || ''

    // TODO: move the styles to another file and update webpack.main.config.js
    const promptStyles = `
    @import url(https://fonts.googleapis.com/css?family=Muli:300,400,700,900|Varela+Round|Lato);

    body {
      margin: 32px 30px;
      font-family: 'Varela Round', sans-serif;
      font-size: 16px;
    }

    h1 {
      text-align: center;
      font-weight: 400;
      letter-spacing: 4px;
      color: #1c1c1c;
    }

    label,
    input,
    button {
      display: block;
      text-align: center;
    }

    button {
      width: 350px;
      height: 60px;
      margin: 10px auto;

      cursor: pointer;
      outline: none;

      background: #252525;
      color: #fff;
      -webkit-appearance: none;
      appearance: none;
      font-size: 22px;
      text-transform: uppercase;
      letter-spacing: 5px;
      box-shadow: 2px 2px 10px 0 rgba(0, 0, 0, 0.5);
      border: solid 1px #202020;
      border-radius: 100px;
    }

    label {
      font-family: 'Varela Round', sans-serif;
      margin: 10px auto 20px;
      font-size: 18px;
      line-height: 1.5em;
    }

    input {
      display: block;
      width: 350px;
      height: 60px;
      margin: 10px auto;

      outline: none;
      font-size: 24px;
      border: 1px solid lightgray;
      border-radius: 100px;
    }
    `

    // TODO: translation i18n
    const promptHtml =
      '<h1>' +
      arg.title +
      '</h1>' +
      '<label for="val">' +
      arg.description +
      '</label>' +
      '<input id="val" value="' +
      arg.val +
      '" autofocus />' +
      "<button onclick=\"require('electron').ipcRenderer.send('prompt-response', document.getElementById('val').value);window.close()\">Confirm</button>" +
      '<style>' +
      promptStyles +
      '</style>'

    promptWindow.loadURL('data:text/html,' + promptHtml)
    promptWindow.show()
    promptWindow.on('closed', function () {
      eventRet.returnValue = promptResponse
      promptWindow = null
    })
  })

  ipcMain.on('prompt-response', function (event, arg) {
    if (arg === '') {
      arg = null
    }
    promptResponse = arg
  })
}

const prompt = {
  init
}

export default prompt
