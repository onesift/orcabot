const nodemailer = require('nodemailer')
const functions = require('firebase-functions')
const admin = require('firebase-admin')
const lodash = require('lodash')
const Stripe = require('stripe')
const express = require('express')
const moment = require('moment')

admin.initializeApp(functions.config().firebase)

const google = require('googleapis')
const OAuth2 = google.auth.OAuth2

const request = require('request')

const CLIENT_ID =
  '866795017632-q7m5hf727lbnrroo473v3al0609l4rhn.apps.googleusercontent.com'

const CLIENT_SECRET = '_qjzlSYiMwM-kHVUfbOjpjc9'

const oauth2Client = new OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  'https://orca-31b91.firebaseapp.com'
)
  
                                                                                                                     
exports.botHeartbeatMonitor = functions.database
  .ref('/users/{userId}/bot/heartbeat')
  .onWrite(async e => {
    const prev = parseInt(e.data.previous.val())
    const curr = parseInt(e.data.val())
    const delta = curr - prev

    const _30min = 60 * 30 // in seconds for timestamp

    // if bot was out for more than 30 minutes
    if (delta > _30min) {
      // shift all missed events from last 24 hours one day into the future

      const bsRef = e.data.ref.parent.parent.child('botSchedule')

      // get all scheduled tasks
      const scheduled = await bsRef.once('value').then(result => result.val())

      const nowTime = getTime()

      const before24h = nowTime - 60 * 24 // in minutes for weekly timeslots

      // find all scheduled tasks from -24 hours to now
      const toMove = Object.keys(scheduled).filter(key => {
        const k = parseInt(key)
        return k > before24h && k < nowTime
      })

      toMove.forEach(sch => {
        const val = scheduled[sch]
        let newTime = parseInt(sch) + 60 * 24

        // prevent overflow
        if (newTime > 10080) {
          newTime = newTime - 10080
        }

        // copy each task 24 * 60 minutes into the future
        bsRef.child(newTime).set(val)

        // delete old scheduled task
        bsRef.child(sch).set(null)
      })
    }
    return e
  })

exports.authCodeToToken = functions.database
  .ref('/users/{userId}/authCode')
  .onWrite(event => {
    // exit if deletion
    if (!event.data.exists()) {
      return
    }

    const code = event.data.val()

    console.log({ code })

    // authCode is single use
    event.data.ref.set(null)

    return oauth2Client.getToken(code, (err, tokens) => {
      if (err) {
        console.warn(err)
        return err
      }

      console.log({ tokens })

      oauth2Client.setCredentials(tokens)

      // store tokens
      return event.data.ref.parent.child('tokens').set(tokens)
    })
  })


// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// GMAIL SENDER

exports.mail = functions.database
  .ref('/users/{userId}/mail/{mailId}')
  .onWrite(event => sendMail(event))

const sendMail = async event => {
  // exit if deletion
  if (!event.data.exists()) return

  const { userId, mailId } = event.params
  const { to, subject, body, targetId, campaignKey } = event.data.val()
  const uidRef = event.data.ref.root.child('/users').child(userId)

  const fromName = await uidRef
    .child('name')
    .once('value')
    .then(result => result.val())

  const fromEmail = await uidRef
    .child('email')
    .once('value')
    .then(result => result.val())

  const from = '"' + fromName + '" <' + fromEmail + '>'

  console.log({ userId, mailId, from, to, targetId, campaignKey })

  let tokens = await uidRef
    .child('tokens')
    .once('value')
    .then(result => result.val())

  oauth2Client.setCredentials(tokens)

  await oauth2Client.refreshAccessToken(async (_, re) => {
    tokens = re
    return uidRef.child('tokens').update(tokens)
  })

  const transport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      type: 'OAuth2',
      user: fromEmail,
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      accessToken: tokens.access_token
    }
  })

  const generateTextFromHTML = true

  return transport
    .sendMail({
      from,
      to,
      subject,
      html: body,
      generateTextFromHTML
    })
    .then(() => {
      console.log('New email sent to:', to)

      // log timestamp only if email was sent
      return uidRef
        .child('targetEmailSent')
        .child(campaignKey)
        .update({ [targetId]: timestamp() })
    })
}

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// BOT ACTIVITY LOG

exports.activity = functions.database
  .ref('/users/{userId}/botRun/{campaignId}/action/{time}')
  .onWrite(event => updateActivity(event))

const updateActivity = event => {
  // exit if deletion
  if (!event.data.exists()) return

  const activityObject = event.data.val()
  const activityRef = event.data.ref
  const activityTime = event.params.time
  const activityCampaignId = event.params.campaignId
  const activityPopulateRef = activityRef.parent.parent.parent.parent.child(
    'activity'
  )
  const activityPopulateLogsRef = activityPopulateRef.child('logs')
  activityObject.campaignId = activityCampaignId

  return (
    activityPopulateLogsRef.child(activityTime).set(activityObject) &&
    updateActivityCounters(activityPopulateRef, activityObject.action)
  )
}

const updateActivityCounters = async (activityPopulateRef, action) => {
  if (action === 'discovered') {
    const leadsCount = await activityPopulateRef
      .child('leadsCount')
      .once('value')
      .then(result => result.val())
    await activityPopulateRef.child('leadsCount').set(leadsCount + 1)
  }

  const actionsCount = await activityPopulateRef
    .child('actionsCount')
    .once('value')
    .then(result => result.val())
  return activityPopulateRef.child('actionsCount').set(actionsCount + 1)
}

const timestamp = () => (Date.now() / 1000) | 0

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// RESET COUNTERS

// reset count if tracked data changes
const change = (fieldName, dataName) =>
  functions.database
    .ref('/users/{userid}/' + dataName + '/{campaignid}')
    .onWrite(event => {
      const countRef = event.data.ref.parent.parent
        .child('campaigns')
        .child(event.params.campaignid)
        .child(fieldName)

      return countRef.set('').then(() => countRef.set(null))
    })

exports.change = {
  discovered: change('discoveredCount', 'targetDiscovered'),
  visited: change('visitedCount', 'targetVisited'),
  connect: change('connectCount', 'targetConnect'),
  connected: change('connectedCount', 'targetConnected'),
  email: change('emailCount', 'targetEmail'),
  emailSent: change('emailSentCount', 'targetEmailSent')
}

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// RECOUNT VALUES

// when the count gets deleted, recount
const recount = (fieldName, dataName) =>
  functions.database
    .ref('/users/{userid}/campaigns/{campaignid}/' + fieldName)
    .onWrite(event => {
      if (event.data.exists()) {
        // not a deletion
        return
      }

      const counterRef = event.data.ref

      const collectionRef = counterRef.parent.parent.parent
        .child(dataName)
        .child(event.params.campaignid)

      // Return the promise from counterRef.set() so our function
      // waits for this async event to complete before it exits.
      return collectionRef.once('value').then(records => {
        const count = records.numChildren()
        // store only if not 0 to prevent recreating deleted branches
        if (count > 0) {
          return counterRef.set(count)
        }
        return true
      })
    })

exports.recount = {
  discovered: recount('discoveredCount', 'targetDiscovered'),
  visited: recount('visitedCount', 'targetVisited'),
  connect: recount('connectCount', 'targetConnect'),
  connected: recount('connectedCount', 'targetConnected'),
  email: recount('emailCount', 'targetEmail'),
  emailSent: recount('emailSentCount', 'targetEmailSent')
}

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// PREVENT IMPOSSIBLE SITUATIONS (can't connect to someone not asked for)

// targetConnected is possible only with a target in targetConnect list
exports.targetConnectedTrim = functions.database
  .ref('/users/{userid}/targetConnected/{campaignid}/{target}')
  .onWrite(event => {
    const discovered = event.data.ref.parent.parent.parent
      .child('targetConnect')
      .child(event.params.campaignid)

    return discovered.once('value').then(snapshot => {
      if (!snapshot.hasChild(event.params.target)) {
        event.data.ref.set(null)
      }
    })
  })

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// ALTERNATIVE SOURCE OF EMAIL DATA
// this api is not ready yet

// use API to get email
exports.findThatLead = functions.database
  .ref('/users/{userid}/targetEmailFTL/{campaignid}/{target}')
  .onWrite(event => {
    if (!event.data.exists()) {
      // ignore deletion
      return
    }

    return request(
      {
        method: 'POST',
        url: 'http://api.findthatlead.com/guess/',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `api_key : 'c016cddc4f7cdc27f7820e1056cf69b1',
name : 'gerard',
surname :   'compte',
domain : 'findthatlead.com'`
      },
      (error, response, body) => {
        if (error) {
          console.log('Error:', error)
        }

        if (response) {
          console.log('Status:', response.statusCode)
          console.log('Headers:', JSON.stringify(response.headers))
        }

        if (body) {
          console.log('Response:', body)
          const countRef = event.data.ref.parent.parent.parent
            .child('targetEmailFTL2')
            .child(event.params.campaignid)
            .child(event.params.target)
          return countRef.set(JSON.parse(body))
        }
      }
    )
  })

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// ANALYTICS

/* logFunction stuff  */

const ref = admin.database().ref()
var time = new Date(
  new Date().toLocaleString('en-US', { timeZone: 'America/Los_Angeles' })
)

// Total number of users signed up for beta
// Total number of users daily
// Daily unique users with active campaigns
// Daily unique users with campaigns set up but no active campaigns
// Daily number of users with no campaigns set up
// Daily number of users with campaigns set up
// Daily average number of campaigns set up per user with active campaigns
// Daily average number of campaigns set up per user with no active campaigns

exports.logFunction = functions.https.onRequest((request, response) => {
  var formattedDate =
    time.getFullYear() + '.' + (time.getMonth() + 1) + '.' + time.getDate()

  ref
    .child('users')
    .once('value')
    .then(snap => {
      var dailyNumberOfUsers = snap.numChildren()
      var totalUseresWithCampainsRunning = 0
      var totalUseresWithCampains = 0
      var totalUseresWithCampainsNotRunning = 0

      var dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr = []
      var dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr = []
      // var names = [];

      snap.forEach(user => {
        // dailyNumberOfUsers += 1;
        var value = 0
        if (user.val().campaigns) {
          totalUseresWithCampains += 1
          lodash.map(user.val().campaigns, campaign => {
            if (campaign.state === 'running') {
              value = 1
            }
          })

          if (value) {
            dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr.push(
              user.child('campaigns').numChildren()
            )
          } else {
            dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr.push(
              user.child('campaigns').numChildren()
            )
          }
        }
        totalUseresWithCampainsRunning += value
      })

      return {
        dailyNumberOfUsers: dailyNumberOfUsers,
        totalUseresWithCampainsRunning: totalUseresWithCampainsRunning,
        totalUseresWithCampainsNotRunning: totalUseresWithCampainsNotRunning,
        totalUseresWithCampains: totalUseresWithCampains,
        dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr: dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr,
        dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr: dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr
      }
    })
    .then(data => {
      // Total number of users signed up for beta
      ref.child('beta_signup').once('value').then(snap => {
        var value = snap.numChildren()
        ref
          .child('data_analytics')
          .child('Total_number_of_users_signed_up_for_beta')
          .push({ date: formattedDate, value: value })
        // ref.child("log").child("Total_number_of_users_signed_up_for_beta").remove()//.push({"date": formattedDate, "value": value})
      })

      // Total number of users daily
      // console.log("dailyNumberOfUsers",data.dailyNumberOfUsers);
      ref
        .child('data_analytics')
        .child('Total_number_of_users_daily')
        .push({ date: formattedDate, value: data.dailyNumberOfUsers })

      // Daily unique users with active campaigns
      // console.log("totalUseresWithCampainsRunning",data.totalUseresWithCampainsRunning);
      ref
        .child('data_analytics')
        .child('Daily_unique_users_with_active_campaigns')
        .push({
          date: formattedDate,
          value: data.totalUseresWithCampainsRunning
        })

      // Daily unique users with campaigns set up but no active campaigns
      // console.log("totalUseresWithCampainsNotRunning",data.totalUseresWithCampains - data.totalUseresWithCampainsRunning);
      ref
        .child('data_analytics')
        .child(
          'Daily_unique_users_with_campaigns_set_up_but_no_active_campaigns'
        )
        .push({
          date: formattedDate,
          value:
            data.totalUseresWithCampains - data.totalUseresWithCampainsRunning
        })

      // Daily number of users with no campaigns set up
      // console.log("numberOfUsersWithNoCampaignsSetUp",data.dailyNumberOfUsers - data.totalUseresWithCampains);
      ref
        .child('data_analytics')
        .child('Daily_number_of_users_with_no_campaigns_set_up')
        .push({
          date: formattedDate,
          value: data.dailyNumberOfUsers - data.totalUseresWithCampains
        })

      // Daily number of users with campaigns set up
      // console.log("totalUseresWithCampains",data.totalUseresWithCampains);
      ref
        .child('data_analytics')
        .child('Daily_number_of_users_with_campaigns_set_up')
        .push({ date: formattedDate, value: data.totalUseresWithCampains })

      // console.log("dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr",data.dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr);
      // console.log("dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr",data.dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr);

      // Daily average number of campaigns set up per user with active campaigns
      // console.log("dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaigns", lodash.meanBy(data.dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr));
      ref
        .child('data_analytics')
        .child(
          'Daily_average_number_of_campaigns_set_up_per_user_with_active_campaigns'
        )
        .push({
          date: formattedDate,
          value: lodash.meanBy(
            data.dailyAverageNumberOfCampaignsSetUpPerUserWithActiveCampaignsArr
          )
        })

      // Daily average number of campaigns set up per user with no active campaigns
      // console.log("dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaigns", lodash.meanBy(data.dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr));
      ref
        .child('data_analytics')
        .child(
          'Daily_average_number_of_campaigns_set_up_per_user_with_no_active_campaigns'
        )
        .push({
          date: formattedDate,
          value: lodash.meanBy(
            data.dailyAverageNumberOfCampaignsSetUpPerUserWithNoActiveCampaignsArr
          )
        })
    })

  response.send('logFunction')
})

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// BOT SCHEDULER

const timeslotVal = async ({ timeslot, event }) =>
  event.data.ref.parent.parent.parent
    .child('botSchedule')
    .child(timeslot)
    .once('value')
    .then(_ => _.val())

// get value
const getVal = async ({ field, name, event }) =>
  event.data.ref.root
    .child('bot')
    .child(name)
    .child(field)
    .once('value')
    .then(_ => _.val())

const varianceVal = async ({ name, event }) =>
  getVal({ field: 'variance', name, event })

const offsetVal = async ({ name, event }) =>
  getVal({ field: 'offset', name, event })

// get minute number of the week for scheduler
const getTime = () => {
  const d = new Date()
  return d.getDay() * 24 * 60 + d.getHours() * 60 + d.getMinutes()
}

// update list of running campaigns
exports.campaignRunning = functions.database
  .ref('/users/{userid}/campaigns/{campaignid}/state')
  .onWrite(async event => {
    const written = event.data.val()

    const indexRef = event.data.ref.parent.parent.parent
      .child('campaignRunning')
      .child(event.params.campaignid)

    // if subscription has expired, change the state of the campaign to paused
    const subscriptionExpiresRef = event.data.ref.parent.parent.parent.child(
      'subscriptionExpires'
    )
    await subscriptionExpiresRef.once('value').then(async result => {
      const expires = moment(parseInt(result.val(), 10))
      if (expires.isBefore(Date.now())) {
        if (written === 'running') {
          await event.data.ref.set('paused')
        }
        return indexRef.remove()
      }
    })

    if (written === 'running') {
      // add campaign to list of running campaigns
      return indexRef.set(true)
    }

    // remove campaign from list of running campaigns
    return indexRef.remove()
  })

// add a task to bot schedule
const addBotSchedule = async ({ offset, variance, macro, event }) => {
  if (!offset) {
    // if offset not provided, get default from db
    offset = await offsetVal({ name: macro, event })
  }

  if (offset <= 0) {
    offset = 30
  }

  if (!variance) {
    // if variance not provided, get default from db
    variance = await varianceVal({ name: macro, event })
  }

  if (variance <= 0) {
    variance = 20
  }

  if (macro === 'visitConnect') {
    const offset2 = await getVal({ field: 'offset2', name: macro, event })
    const variance2 = await getVal({ field: 'variance2', name: macro, event })
  }

  const time = getTime()

  // offset in minutes from now
  let later = time + offset

  // add variance
  if (variance) {
    later = later + (((Math.random() - 0.5) * 2 * variance) | 0)
  }

  // prevent overflow
  if (later > 10080) {
    later = later - 10080
  }

  const { campaignid, target, page } = event.params

  let timeslot = event.data.ref.parent.parent.parent
    .child('botSchedule')
    .child(campaignid)

  if (timeslot) {
    timeslot = timeslot.child(later)
  }

  let data = [macro]

  if (target) {
    data.push(target)
  }

  if (macro === 'search') {
    data.push(page) // from which page the bot is starting next command's searching
    data.push(page === 1 ? 1 : 5) // how many result pages will be viewed in command FIXME: add a variance to 5 (4-6 of result pages to visit?)
    data.push(event.data.val()) // the URI for the search results
  }

  // wrap task with bot start/end and merge parameters
  const task = 'start,macro(' + data.join(';') + '),end'

  const existing = await timeslotVal({ timeslot: later, event })

  if (existing === null) {
    // if no existing data
    await timeslot.set(task)
  } else {
    let emptyFound = false
    // try to find an empty slot +-2 hours
    for (let i = 0; i < 120; i++) {
      // look both ways

      // positive offset
      if (!await timeslotVal({ timeslot: later + i, event })) {
        // found empty slot
        await event.data.ref.parent.parent.parent
          .child('botSchedule')
          .child(later + i)
          .set(task)

        emptyFound = true
        break // done, exit loop
      }

      if (later - i > time) {
        // negative offset if after now
        if (!await timeslotVal({ timeslot: later - i, event })) {
          // found empty slot
          await event.data.ref.parent.parent.parent
            .child('botSchedule')
            .child(later - i)
            .set(task)

          emptyFound = true
          break // done, exit loop
        }
      }
    }

    if (!emptyFound) {
      // append to existing
      await timeslot.set(existing + ',' + task)
    }
  }
}

let offset = null
let variance = null

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// BOT SCHEDULER DATABASE TRIGGERS

// trigger when target added to targetDiscovered
exports.targetDiscoveredScheduler = functions.database
  .ref('/users/{userid}/targetDiscovered/{campaignid}/{target}')
  .onWrite(async event => {
    const { campaignid, target, userid } = event.params

    // store the scheduling of the 0th element of chain for target
    await event.data.ref.root
      .child('users')
      .child(userid)
      .child('botStep')
      .child(campaignid)
      .child(target)
      .child('0')
      .set(timestamp())

    // schedule 0th element of chain
    const chain0 = await event.data.ref.root
      .child('users')
      .child(userid)
      .child('campaigns')
      .child(campaignid)
      .child('pipeline')
      .child('0')
      .once('value')
      .then(_ => _.val())

    await addBotSchedule({ macro: chain0, offset, variance, event })
    // await addBotSchedule({ macro: 'visitConnect', offset, variance, event })
    return event
  })

// get list of bot steps allready done
const getBotStepsDone = async ({ data, userid, campaignid, target }) =>
  data.ref.root
    .child('users')
    .child(userid)
    .child('botStep')
    .child(campaignid)
    .child(target)
    .once('value')
    .then(_ => _.val())

// get campaign's pipeline
const getPipeline = async ({ data, userid, campaignid, target }) =>
  data.ref.root
    .child('users')
    .child(userid)
    .child('campaigns')
    .child(campaignid)
    .child('pipeline')
    .once('value')
    .then(_ => _.val())

const getNextEventInChain = async ({
  data,
  userid,
  campaignid,
  numStepsDone
}) =>
  data.ref.root
    .child('users')
    .child(userid)
    .child('campaigns')
    .child(campaignid)
    .child('pipeline')
    .child(numStepsDone)
    .once('value')
    .then(_ => _.val())

// log step as done
const storeScheduling = async ({
  data,
  userid,
  campaignid,
  target,
  numStepsDone
}) =>
  data.ref.root
    .child('users')
    .child(userid)
    .child('botStep')
    .child(campaignid)
    .child(target)
    .child(numStepsDone)
    .set(timestamp())

const checkPipeline = async event => {
  const { data } = event
  const { campaignid, target, userid } = event.params

  // get pipeline of campaign
  const pipeline = await getPipeline({ data, userid, campaignid, target })

  // get activity of target pipeline
  const botStepsDone = await getBotStepsDone({
    data,
    userid,
    campaignid,
    target
  })

  // get number of steps that are done
  const numStepsDone = Object.keys(botStepsDone).length

  // schedule next event in chain (numSteps = next because zero based array)
  const nextEventInChain = await getNextEventInChain({
    data,
    userid,
    campaignid,
    numStepsDone
  })

  if (!nextEventInChain) {
    // if no event, we are at the end of pipeline, eject
    return event
  }

  // add macro to the schedule
  await addBotSchedule({ macro: nextEventInChain, offset, variance, event })

  // store the scheduling of the n-th element of chain for target
  await storeScheduling({ data, userid, campaignid, target, numStepsDone })

  console.log({
    campaignid,
    target,
    pipeline,
    botStepsDone,
    numStepsDone: numStepsDone,
    nextEventInChain
  })

  return event
}

exports.targetVisitedScheduler = functions.database
  .ref('/users/{userid}/targetVisited/{campaignid}/{target}')
  .onWrite(checkPipeline)

exports.targetConnectedScheduler = functions.database
  .ref('/users/{userid}/targetConnected/{campaignid}/{target}')
  .onWrite(checkPipeline)

// do not autoschedule onConnect event, wait onConnected to trigger
// exports.targetConnectScheduler = functions.database
//  .ref('/users/{userid}/targetConnect/{campaignid}/{target}')
//  .onWrite(checkPipeline)

exports.targetChatScheduler = functions.database
  .ref('/users/{userid}/targetChat/{campaignid}/{target}')
  .onWrite(checkPipeline)

exports.targetEmailScheduler = functions.database
  .ref('/users/{userid}/targetEmail/{campaignid}/{target}')
  .onWrite(checkPipeline)

exports.targetSearchScheduler = functions.database
  .ref('/users/{userid}/campaigns/{campaignid}/url')
  .onWrite(async event => {
    event.params.page = 1
    await addBotSchedule({ macro: 'search', offset: 1, variance: 1, event })
    return event
  })

exports.targetSearchExtendedScheduler = functions.database
  .ref('/users/{userid}/campaigns/{campaignid}/searchResultPageCount')
  .onWrite(async event => {
    const pageCount = event.data.val()
    // schedule all pages starting from 2nd
    for (let i = 2; i <= pageCount; i++) {
      event.params.page = i
      await addBotSchedule({
        macro: 'search',
        offset: (await offsetVal({ name: 'search', event })) * i,
        variance,
        event
      })
    }
    return event
  })

// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// NEW USER

// init user data on first login
exports.newRegisteredUser = functions.database
  .ref('/users/{userid}/email')
  .onCreate(async event => {
    const WEEK_IN_MILLISECONDS = 7 * 24 * 60 * 60 * 1000 // days, hours, minutes, seconds
    const TWO_WEEKS_IN_MILLISECONDS = WEEK_IN_MILLISECONDS * 2

    const subscriptionTrialRef = event.data.ref.parent.child(
      'subscriptionTrial'
    )
    const subscriptionEnabledRef = event.data.ref.parent.child(
      'subscriptionEnabled'
    )
    const subscriptionExpiresRef = event.data.ref.parent.child(
      'subscriptionExpires'
    )

    await subscriptionTrialRef.set(true)
    await subscriptionEnabledRef.set(false)
    await subscriptionExpiresRef.set(Date.now() + TWO_WEEKS_IN_MILLISECONDS)

    return event
  })

const getEmail = async uid =>
  await admin
    .database()
    .ref('users/' + uid + '/email')
    .once('value')
    .then(_ => _.val())


// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// STRIPE API FOR BILLING

const _stripeCharge = express()
_stripeCharge.get('/:uid/:stripeToken', async (req, res) => {
  const MONTH_IN_MILLISECONDS = 30 * 24 * 60 * 60 * 1000 // days, hours, minutes, seconds
  const { customers, subscriptions } = Stripe(
    'sk_test_vefRZxPrggSqQaEMJ9Azheu1'
  )
  const { stripeToken, uid } = req.params

  const email = await getEmail(uid)

  // create a customer
  const customer = await customers.create({ email })
  await admin.database().ref('users/' + uid + '/stripe/customer').set(customer)

  // use token of the credit card
  const source = await customers.createSource(customer.id, {
    source: stripeToken
  })
  await admin.database().ref('users/' + uid + '/stripe/source').set(source)
  await admin.database().ref('users/' + uid + '/cards/' + source.id).set(source)

  // subscribe customer to the plan
  const subscription = await subscriptions.create({
    customer: customer.id,
    items: [{ plan: 'basic' }],
    trial_period_days: 14
  })
  await admin
    .database()
    .ref('users/' + uid + '/stripe/subscription')
    .set(subscription)

  await admin.database().ref('users/' + uid + '/subscriptionTrial').set(true)
  await admin.database().ref('users/' + uid + '/subscriptionEnabled').set(true)

  const expiresRef = admin
    .database()
    .ref('users/' + uid + '/subscriptionExpires')

  await expiresRef.once('value').then(async result => {
    const subscriptionExpires = parseInt(result.val(), 10)
    if (moment(subscriptionExpires).isAfter(Date.now())) {
      await expiresRef.set(subscriptionExpires + MONTH_IN_MILLISECONDS)
    } else {
      await expiresRef.set(Date.now() + MONTH_IN_MILLISECONDS)
    }
  })

  return res.status(200).send('')
})

exports.stripeCharge = functions.https.onRequest(_stripeCharge)
