# Firebase functions #

Source is in the `functions/src` folder.

Source gets translated from typescript to JS to get await async support.

## Deployment ##

For deploying to the Firebase server use:

```yarn deploy```