import utils from '../utils'

describe('Launch', function () {
  beforeEach(utils.beforeEach)
  afterEach(utils.afterEach)

  it('shows the window', function () {
    return this.app.browserWindow.isVisible()
      .then(visible => {
        expect(visible).to.beTrue
      })
  })
})
