import Vue from 'vue'
import store from './../../app/src/renderer/vuex/store'

export {
  Vue,
  store,
}
