import { Vue, store } from '../test_helper'

import Welcome from 'renderer/components/Welcome'

describe('Welcome.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      store: store,
      render: h => h(Welcome)
    }).$mount()

    expect(vm.$el.querySelector('h4.title').textContent).to.contain('Engage and nuture leads automatically.')
  })
})
