import { QUEUE, addToQueue, getFromQueue } from 'renderer/bot/queue'

describe('QUEUE', () => {
  it('should be empty on start', () => {
    expect(QUEUE.length).to.equal(0)
  })

  context('addToQueue', () => {
    it('should throw error if argument is not Object', () => {
      let errorRegexp = /^Only objects can be added to QUEUE\.$/
      expect(() => addToQueue([])).to.throw(Error, errorRegexp)
      expect(() => addToQueue(1)).to.throw(Error, errorRegexp)
      expect(() => addToQueue('string')).to.throw(Error, errorRegexp)
      expect(() => addToQueue(false)).to.throw(Error, errorRegexp)
      expect(() => addToQueue(() => {})).to.throw(Error, errorRegexp)
    })
    it('should throw error if action name is not defined or is empty', () => {
      let notDefinedRegexp = /^Action name must be defined\.$/
      expect(() => addToQueue({})).to.throw(Error, notDefinedRegexp)
      expect(() => addToQueue({action: ''})).to.throw(Error, notDefinedRegexp)
    })
    it('should throw error if action name is not in available actions', () => {
      let unknownRegexp = /^Unknown action someaction\.$/
      expect(() => addToQueue({action: 'someaction'})).to.throw(Error, unknownRegexp)
    })
    it('should add items to QUEUE', () => {
      addToQueue({action: 'view'})
      addToQueue({action: 'connect'})
      expect(QUEUE.length).to.equal(2)
    })
    it('should preserve order how items are added to QUEUE', () => {
      expect(QUEUE[0].action).to.equal('view')
      expect(QUEUE[1].action).to.equal('connect')
    })
    it('should throw error if we will exceed daily limit for this action', () => {
      // daily limit for VIEW action is 10
      // add 9 more view actions to QUEUE as we have one added above
      Array(...Array(9)).forEach(() => {
        addToQueue({action: 'view'})
      })
      let exceedRegexp = /^You will exceed daily VIEW limit. You already have 10 items in QUEUE\.$/
      expect(() => addToQueue({action: 'view'})).to.throw(Error, exceedRegexp)
    })
  })

  context('getFromQueue', () => {
    it('should return first item from QUEUE', () => {
      let item = getFromQueue()
      expect(item.action).to.equal('view')
    })
    it('should remove item from QUEUE', () => {
      getFromQueue()
      expect(QUEUE.length).to.equal(9)
    })
    it('should throw error if QUEUE is empty', () => {
      // remove all items from QUEUE
      Array(...Array(9)).forEach(() => {
        getFromQueue()
      })
      expect(() => getFromQueue(() => {})).to.throw(Error, 'QUEUE is empty.')
    })
  })
})
