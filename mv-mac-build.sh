#!/bin/bash
# Invoke this script in main app dir, for example:
# mv-mac-build.sh
#
# install dependecies:
# yarn global add underscore-cli

version=`cat "package.json" | underscore select ".version" --outfmt=text`
echo "Build version ${version}"

mv "dist/mac/Orca.app.zip" "dist/orca-"${version}"-mac.zip"
mv "dist/mac/Orca.dmg" "dist/orca-"${version}".dmg"