# orca

> Marketing orchestration tool

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:9080
$ yarn dev

# build electron app for production
$ yarn build
$ yarn build:win # only Windows app for production
$ yarn build:mac # only Mac signed app for production

# build electron app without compression and signing for testing
$ yarn build:win-test # only Windows fast uncompressed build
$ yarn build:mac-test # only Mac fast uncompressed build without signing

# deploy app to Amazon S3
$ yarn deploy # deploys default version for current system
$ yarn deploy:mac # deploys Mac version
$ yarn deploy:win # deploys Windows version

# run webpack in production
$ yarn pack

# run tests (both unit and e2e)
$ yarn test

```

More information can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/docs/npm_scripts.html).
